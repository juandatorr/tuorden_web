import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { ServicioLayoutComponent } from './layouts/servicio-layout/servicio-layout.component';

const routes: Routes = [
  /* {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  }, */
  /*  {
    path: '',
    component: AdminLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: './layouts/admin-layout/admin-layout.module#AdminLayoutModule'
      }
    ]
  }, */
  /*   {
    path: '',
    component: AuthLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: './layouts/auth-layout/auth-layout.module#AuthLayoutModule'
      }
    ]
  },  */
      {
        path: '',
        loadChildren: './layouts/servicio-layout/servicio-layout.module#ServicioLayoutModule'
      },
      {
        path: 'home',
        loadChildren: './layouts/home-layout/home-layout.module#HomeLayoutModule'
      }
  /* {
    path: '**',
    redirectTo: 'home'
  } */
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes,{
      useHash: true
    })
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
