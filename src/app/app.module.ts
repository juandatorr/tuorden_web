import { HeaderComponent } from './shared/components/header/header.component';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { UiSwitchModule } from 'ngx-ui-switch';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';
import { HomeLayoutComponent } from './layouts/home-layout/home-layout.component';
import { ServicioLayoutComponent } from './layouts/servicio-layout/servicio-layout.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';
import { ModalMapsComponent } from './shared/components/modal-maps/modal-maps.component';
import { AgmCoreModule } from '@agm/core';
import { CommonModule, CurrencyPipe } from '@angular/common';
import { NgxUiLoaderModule,
  NgxUiLoaderConfig,
  NgxUiLoaderHttpModule,
  SPINNER,
  POSITION,
  PB_DIRECTION } from 'ngx-ui-loader';
import { HomeComponent } from './pages/home/home.component';
import { SidebarModule } from 'ng-sidebar';
import { ServicioComponent } from './pages/servicio/servicio.component';
import { NgImageSliderModule } from 'ng-image-slider';
import { FooterComponent } from './shared/components/footer/footer.component';
import { InterceptorHttpService } from './shared/services/interceptor-http.service';

const ngxUiLoaderConfig: NgxUiLoaderConfig = {
  bgsColor: '#ff0000',
  fgsPosition: POSITION.centerCenter,
  fgsColor : '#eb0000',
  fgsSize: 40,
  fgsType: SPINNER.squareJellyBox,
  pbColor: 'red',
  pbDirection: PB_DIRECTION.leftToRight,
  pbThickness: 5
}

@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    ComponentsModule,
    NgbModule,
    RouterModule,
    AppRoutingModule,
    ReactiveFormsModule,
    CommonModule,
    UiSwitchModule,
    AngularMultiSelectModule,
    SidebarModule.forRoot(),
    // NgxUiLoaderModule,
    NgxUiLoaderModule.forRoot(ngxUiLoaderConfig),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBt68PEyeET9JkgAGVqtPbVRXb6IXbU0E4',
      libraries: ['places', 'drawing','geometry']
    }),
    NgImageSliderModule
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    AuthLayoutComponent,
    HomeLayoutComponent,
    ModalMapsComponent,
    HomeComponent,
    ServicioLayoutComponent,
    ServicioComponent,
    HeaderComponent,
    FooterComponent
  ],
  providers: [CurrencyPipe,
    { provide: HTTP_INTERCEPTORS, useClass: InterceptorHttpService, multi: true }
  ],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
