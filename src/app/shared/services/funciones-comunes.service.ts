import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import swal from 'sweetalert2';
import {CurrencyPipe} from '@angular/common';

@Injectable({
    providedIn: 'root'
})
export class FuncionesComunesService {
    constructor(private currencyPipe: CurrencyPipe) {
    }

    toast() {
        return swal.mixin({
            toast: true,
            position: 'bottom-start',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            onOpen: (toast) => {
                toast.addEventListener('mouseenter', swal.stopTimer);
                toast.addEventListener('mouseleave', swal.resumeTimer);
            }
        });
    }

    formatoDineroTypeScript() {
        return Intl.NumberFormat('es-Co', {
            style: 'currency',
            currency: 'COP',
            minimumSignificantDigits: 1
        });
    }

    formatoNumberTypeScript() {
        return Intl.NumberFormat('es-Co');
    }

    formatoCurrencyPipe(valor) {
        return this.currencyPipe.transform(valor, 'COP', '$', '1.0');
    }

    // Funciones para funcionamiento de swal personalizado
    swalDosBotones(titulo, text, type, confirmButtonText, cancelButtonText, cancelButtonColor?, confirmButtonColor?) {
        return swal.fire({
            title: titulo,
            text: text,
            icon: type,
            showCancelButton: true,
            confirmButtonColor: confirmButtonColor ? confirmButtonColor : '#3085d6',
            cancelButtonColor: cancelButtonColor ? cancelButtonColor : '#d33',
            confirmButtonText: confirmButtonText,
            cancelButtonText: cancelButtonText,
            allowEscapeKey: false,
            allowEnterKey: false,
            allowOutsideClick: false,
        });
    }

    mostrarSwal(titulo, mensaje, icono) {
        swal.fire({
            title: titulo,
            text: mensaje,
            icon: icono,
            confirmButtonText: 'Ok',
            allowEscapeKey: false,
            allowEnterKey: false,
            allowOutsideClick: false,
        });
    }

    mostrarCargando(titulo) {
        swal.fire({
            title: titulo,
            onBeforeOpen: () => {
                swal.showLoading();
            }
        });
    }

    cerrarCargando() {
        if (swal.isVisible()) {
            swal.close();
        }
    }

    obtenerHeight(cantidad) {
        return (window.innerHeight) - cantidad;
    }

    obtenerHeightCompleto(cantidad) {
        return (window.innerHeight) - ((window.innerHeight) - cantidad);
    }

    filtrarDatos(arregloCompleto, busqueda, datosFiltrar) {
        return arregloCompleto.filter(
            function(x) {
                let resultado = false;
                for (let i = 0; i < datosFiltrar.length; i++) {
                    const dato = datosFiltrar[i];

                    if (eval(dato).toLowerCase().indexOf(busqueda.toLowerCase()) > -1) {
                        //console.log(eval(dato));
                        resultado = true;
                        break;
                    }
                }
                return resultado;
            }
        );
    }
}
