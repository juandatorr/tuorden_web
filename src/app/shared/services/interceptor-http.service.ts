import { Injectable } from '@angular/core';
import {HttpHandler, HttpInterceptor, HttpHeaders, HttpRequest, HttpEvent, HttpResponse, HttpErrorResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import {tap} from 'rxjs/internal/operators';
import {Router} from '@angular/router';
import swal from "sweetalert2";

@Injectable()
export class InterceptorHttpService implements HttpInterceptor {

    constructor(private router: Router) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let headers: any;
        // Si el usuario está logueado, de lo contrario no adjuntar estos headers
        // pues puede que el endpoint que estamos llamando es el Login o Register
        // los cuales no requerir que el usuario esté autenticado, sería estupido.
        if (localStorage.getItem('apilogin')) {
            // Adjuntamos los headers a la petición
            const apiLogin = localStorage.getItem('apilogin');
            const apiKey = localStorage.getItem('apikey');

            headers = new HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'apilogin' : apiLogin,
                'apikey': apiKey
            });

        }
        const cloneReq = request.clone({headers});

        return next.handle(cloneReq).pipe(
            tap( event => {
                if (event instanceof HttpResponse) {

                }
            }, error => {
                if (error.status === 401) {
                    swal.fire(
                        'Error',
                        'Llegaste al limite de tiempo de inactividad, debes volver a ingresar',
                        'error'
                    ).then((result) => {
                        localStorage.clear();
                        this.router.navigate(['/servicio']);
                    });
                }
            })
        );
    }
}
