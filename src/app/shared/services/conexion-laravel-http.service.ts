import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ConexionLaravelHttpService {
    versionPlataforma = '1.1.1';

    endPoint = '';
    endPointAntigua = 'https://tuorden.com.co/api/app/v1.18/';
    debug = localStorage.getItem('debug');


    constructor(private http: HttpClient) {
        this.getConexion();
    }

    public getConexion() {
         return this.http.get('./assets/configuracion/conexion.json?' + (new Date()).getTime()).subscribe(
             resp => {
                 localStorage.setItem('endPoint', JSON.parse(JSON.stringify(resp)).endPoint);
                 localStorage.setItem('debug', JSON.parse(JSON.stringify(resp)).debug);
                 this.versionPlataforma = JSON.parse(JSON.stringify(resp)).versionPlataforma;
                 this.endPoint = JSON.parse(JSON.stringify(resp)).endPoint;
                 this.debug = JSON.parse(JSON.stringify(resp)).debug;
             }
         );
    }

    get(url: any, antigua?): Observable<any> {
        this.endPoint = localStorage.getItem('endPoint');
        if (this.endPoint === null) {
            this.endPoint = 'https://pruebaslaravel.tuorden.com.co/';
        }
        if (antigua) {
            return this.http.get(this.endPointAntigua + url);
        } else {
            return this.http.get(this.endPoint + url);
        }

    }

    post(url: any, datos: any, antigua?): Observable<any> {
        if (this.endPoint === '') {
            this.endPoint = localStorage.getItem('endPoint');
        }
        if (antigua) {
            return this.http.post(this.endPointAntigua + url, datos);
        } else {
            return this.http.post(this.endPoint + url, datos);
        }
    }

    postPermisos(url: any, datos: any): any {
        if (this.endPoint === '') {
            this.endPoint = localStorage.getItem('endPoint');
        }

        const respuesta = new Promise<boolean>(resolve =>
            this.http.post(this.endPoint + url, datos)
                .subscribe(resp => {
                    resolve(false);
                })
        );
        //console.log(respuesta);
    }

    put(url: any, datos: any): Observable<any> {
        if (this.endPoint === '') {
            this.endPoint = localStorage.getItem('endPoint');
        }
        return this.http.put(this.endPoint + url, datos);
    }

    patch(url: any, datos: any): Observable<any> {
        if (this.endPoint === '') {
            this.endPoint = localStorage.getItem('endPoint');
        }
        return this.http.patch(this.endPoint + url, datos);
    }

    delete(url: any): Observable<any> {
        if (this.endPoint === '') {
            this.endPoint = localStorage.getItem('endPoint');
        }
        return this.http.delete(this.endPoint + url);
    }

    getVersionPlataforma() {
        return this.versionPlataforma;
    }
}
