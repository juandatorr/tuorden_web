import { MapsAPILoader } from '@agm/core';
import { Component, OnInit, AfterViewInit, Output, EventEmitter, ViewChild, TemplateRef } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ConexionLaravelHttpService } from '../../services/conexion-laravel-http.service';
import { FuncionesComunesService } from '../../services/funciones-comunes.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, AfterViewInit {

  @ViewChild('modalLogin', {static: true}) modalLogin: TemplateRef<any>;
  @ViewChild('modalBanner', {static: true}) modalBanner: TemplateRef<any>;


  @Output() funcionParaEmitir: EventEmitter<any> = new EventEmitter<any>();
  @Output() visualizarEmit: EventEmitter<any> = new EventEmitter<any>();
  @Output() cerrarSessionEmit: EventEmitter<any> = new EventEmitter<any>();

  // Variables de dirección
  direcciones = [];
  direccionSeleccionada = [];
  // Fin variables dirección

  // Variable usuario
  usuario = null;
  // Fin variable usuario

  // Variable modal direccion
  ciudades = [];
  latitud;
  longitud;
  coordenadas;
  visualizarServicios = true;
  // Fin variable modal dirección

  // Variable modal direccion
  transacciones = [];
  // Fin variable modal dirección

  // Variable modal ordenes
  ordenes = [];
  // Fin Variable modal ordenes

  // Atributo Comercio
  comercioCerrado = true;
  horarioComercioInicio;
  horarioComercioFin;
  // Fin Atributo Comercio

  // Form para crear dirección
  form_creacion_direccion  = this.fb.group({
    ciudad : ['', Validators.required],
    direccion: ['', Validators.required],
    nombre : ['', Validators.required],
    descripcion : ['', Validators.required],
    latitud : [''],
    longitud : [''],
  });
  // Fin Form para crear dirección

  // Variables para iniciar session un usuario
  login = true;

  // Variables opcion celular
  celular = false;
  numero_celular = null;
  verificar_codigo_celular = false;
  // Fin Variables opcion celular

  // Variables opción correo
  correo = false;
  verificar_codigo_correo = false;
  correo_electronico = null;
  // Fin Variables opción correo

  // Variables tiempo reenvio
  tiempo_reenvio = 300;
  // Fin Variables tiempo reenvio

  // Form para crear codigo celular
  form_validar_celular = this.fb.group({
    celular: ['', [Validators.required, Validators.minLength(10)]],
    codigo : [''],
  });
  // Fin Form para crear codigo celular

  // Form para crear codigo correo
  form_validar_correo  = this.fb.group({
    correo: ['', Validators.required],
    codigo : [''],
    usuario_id : [''],
  });
  // Fin Form para crear codigo correo

  // Form para crear codigo usuario
  form_creacion_usuario = this.fb.group({
    nombre : ['', Validators.required],
    apellido: ['', Validators.required],
    correo : ['', Validators.required],
  });
    // Fin Form para crear usuario

  // Variable para aceptar terminos
  aceptarTerminos = false;
  // Fin Variable para aceptar terminos

  proveedorId = 1;
  mostrarMapa = true;

  // Variable configuración select
  configuracionSelect = {};

  // Variable Carrito de compras
  carritoCompras: any = {celular: '', comentario: '', comercio: {
    comentarios: true,
    descripcion: '',
    direccion: '',
    fecha_modificacion: '',
    id: '',
    latitud: '',
    longitud: '',
    marketplace: false,
    nombre: '',
    sede_id: '',
    takeOut: false,
    valor_domicilio: ''
}, direccion: {
              direccion: '',
              descripcion: '',
              id: '',
              latitud: '',
              longitud: '',
              nombre: ''
},
estado_entrega: 0,
fecha_programacion: '',
id: 0,
marketplace: false,
pago: 1,
productos: [],
propina : 0,
servicio_id : 1,
take_out : false,
total: 0,
tu_pago : 0,
sistema: {
app: {
      builType : 'release',
      version : 0,
},
os: {
      name: '',
      model: '',
      sdk: '',
      version: ''
}
}
};

// visualizarServicio = 'true';

// Variable apiKey y apiLogin
apiKey = null;
apiLogin = null;
// Fin Variable apiKey y apiLogin

// Fin Variable Carrito de compras
  constructor(public modalService: NgbModal,
              public config: NgbModalConfig,
              public funciones: FuncionesComunesService,
              public fb: FormBuilder,
              private http: ConexionLaravelHttpService,
              public router: Router,
              private ngxService: NgxUiLoaderService,
              private mapsAPILoader: MapsAPILoader
  ) {
    this.cargarSelect();
   }

  ngOnInit() {

    this.visualizar();
    this.apiKey = localStorage.getItem('apikey');
    this.apiLogin = localStorage.getItem('apilogin');
    if (this.apiKey !== null && this.apiLogin !== null) {
      this.ngxService.start();
      // this.http.get('usuario?apiKey=' + this.apiKey + '&apiLogin=' + this.apiLogin, true).subscribe(
        this.http.get('usuario', true).subscribe(
        response => {
              if ( !response.error) {
                this.usuario = response.message;
                localStorage.setItem('usuario', JSON.stringify(this.usuario));
                this.proveedorId = 1;

                if  (this.usuario != null) {
                  this.obtenerDirecciones(false);
                }

                const usuarioJson = localStorage.getItem('usuario');
                const direccionesJson = localStorage.getItem('direcciones');
                const direccionJsonSeleccionada = localStorage.getItem('direccion_seleccionada');

                if (usuarioJson) {
                    this.usuario = JSON.parse(usuarioJson);
                }

                if (direccionesJson) {
                    this.direcciones = JSON.parse(direccionesJson);
                }

                if (direccionJsonSeleccionada) {
                    this.direccionSeleccionada[0] = JSON.parse(direccionJsonSeleccionada);
                    const la = this.direccionSeleccionada[0].latitud;
                    const lo = this.direccionSeleccionada[0].longitud;
                    // this.consultarMenuLatLng(la, lo);
                }
                  this.ngxService.stop();
                } else {
                  this.ngxService.stop();
                }

                // Orden si no existe se crea
                const ordenJson = localStorage.getItem('orden');
                if (ordenJson === null) {
                  this.crearOrden();
                }
                }, err => {
                  this.ngxService.stop();
                }
              );


                // this.visualizarServicio = localStorage.getItem('visualizar');
    } else {
      this.modalService.open(this.modalBanner, {centered: true, size: 'lg'});

    }
  }

  continuarWeb(modalLogin, modalBanner) {
    modalBanner.close();
    this.modalService.open(modalLogin, {centered: true, size: 'lg'});
  }
  cargarSelect() {
    this.configuracionSelect = {
      singleSelection: true,
      text: 'Selecciona una opción',
      classes: 'myclass',
      primaryKey: 'id',
      labelKey: 'nombre',
      noDataLabel: 'No se encontraron datos',
      enableSearchFilter: true,
      enableFilterSelectAll: false,
      searchPlaceholderText: 'Buscar',
      // lazyLoading: true,
    };
  }

  ngAfterViewInit() {
    if (this.usuario != null) {
      if (this.usuario.nombre === '') {
         // this.modalService.open(this.modalCompletarUsuario, {windowClass: 'xlModalMediano', centered: true, size: 'lg'});
      } else {
        if (this.direccionSeleccionada.length === 0) {
         // this.modalService.open(this.modalDirecciones, {windowClass: 'xlModalMediano', centered: true, size: 'lg'});
        }
      }
   }
  }

  // Método general
  public getError(controlName: string, form): string {
    /*let error = '';
    const control = form.get(controlName);
    if (control.errors != null) {
        error = JSON.stringify(control.errors);
    }
    return error;*/
    return '';
  }

  // Método para visualizar los modales del header.
  abrirModales(modal, seleccion) {
    if (seleccion === 'login') {
      this.modalService.open(modal, {centered: true, size: 'lg'});
    }

    if ( seleccion === 'direcciones') {
      this.modalService.open(modal, {windowClass: 'xlModalMediano', centered: true, size: 'lg'});
    }

    if ( seleccion === 'direccion') {
      this.modalService.dismissAll();
      this.cargarCiudades();
      this.modalService.open(modal, {windowClass: 'xlModalMediano', centered: true, size: 'lg'});
    }

    if ( seleccion === 'usuario') {
      this.modalService.open(modal, {centered: true, size: 'lg'});
    }

    if (seleccion === 'transaccion') {
      this.cargarTransacciones();
      this.modalService.open(modal, {centered: true, size: 'lg'});
    }

    if (seleccion === 'pqrs') {
      this.modalService.open(modal, {centered: true, size: 'lg'});
    }

    if (seleccion === 'ordenes') {
      this.ngxService.start();
      this.cargarOrdenesUsuario();
      this.modalService.open(modal, {centered: true, size: 'lg'});
      this.ngxService.stop();
    }
  }


  // Metodos modal direcciones
  cargarCiudades() {
    this.http.get('ciudades', true).subscribe(
      response => {
          this.ciudades = response.message;
      }, err => {

      });
  }
  // Fin Metodos modal direcciones

  // Métodos modal Transacciones
  cargarTransacciones() {
    const apiKey = localStorage.getItem('apikey');
    const apiLogin = localStorage.getItem('apilogin');
    this.http.get('transacciones_tarjetas?apiKey=' + apiKey + '&apiLogin=' + apiLogin, true).subscribe(
      response => {
          if (!response.error) {
            this.transacciones = response.message;
          } else {
            this.funciones.toast().fire({
              icon: 'warning',
              title: 'Error cargando las transacciones'
            });
          }
      }, err => {
        this.funciones.toast().fire({
          icon: 'warning',
          title: 'Error cargando las transacciones'
        });
      });
  }
  // Fin Métodos modal Transacciones

  // Método Ordenes
  cargarOrdenesUsuario() {
    const apiKey = localStorage.getItem('apikey');
    const apiLogin = localStorage.getItem('apilogin');
    this.http.get('ordenes?apiKey=' + apiKey + '&apiLogin=' + apiLogin + '&proveedor_id=' + this.proveedorId, true).subscribe(
      response => {
        if (!response.error) {
          this.ordenes = response.message;
        } else {
          this.funciones.toast().fire({
            icon: 'success',
            title: response.message
          });
        }
      }, err => {
        this.funciones.toast().fire({
          icon: 'warning',
          title: 'Error Consultando Ordenes'
        });
      });
  }
  // Fin Métedo Ordenes

  // Metodo para cerrar la sessión de un usuario
  cerrarSession() {
    this.comercioCerrado = true;
    this.login = true;

    // Atributos opcion celular
    this.celular = false;
    this.numero_celular = null;
    this. verificar_codigo_celular = false;

    // Atributos opción facebook


    // Atributos opción correo
    this.correo = false;
    this.verificar_codigo_correo = false;
    this.correo_electronico = null;

    this.tiempo_reenvio = 300;

    this.mostrarMapa = true;

    this.form_validar_celular = this.fb.group({
      celular: ['', Validators.required],
      codigo : [''],
    });

    this.form_validar_correo  = this.fb.group({
      correo: ['', Validators.required],
      codigo : [''],
      usuario_id : [''],
    });

    this.form_creacion_direccion  = this.fb.group({
      ciudad : ['', Validators.required],
      direccion: ['', Validators.required],
      nombre : ['', Validators.required],
      descripcion : ['', Validators.required],
      latitud : [''],
      longitud : [''],
    });

      this.usuario = null;
      this.direcciones = [];
      this.direccionSeleccionada = [];
      this.aceptarTerminos = false;
      localStorage.clear();
  }
  // Fin Metodo para cerrar la sessión de un usuario

  volverHome() {

    this.login = true;
    // Atributos opcion celular
    this.celular = false;
    this.numero_celular = null;
    this.verificar_codigo_celular = false;

    // Atributos opción facebook


    // Atributos opción correo
    this.correo = false;
    this.verificar_codigo_correo = false;
    this.correo_electronico = null;

    this.tiempo_reenvio = 300;

    this.form_validar_celular = this.fb.group({
      celular: ['', Validators.required],
      codigo : [''],
    });

      this.form_validar_correo  = this.fb.group({
        correo: ['', Validators.required],
        codigo : [''],
        usuario_id : [''],
      });
    }

    // Método visualizar los pasos para iniciar session.
    menuOpciones(seleccion) {
      if (!this.aceptarTerminos) {
        this.funciones.mostrarSwal(
          'Error',
          'Debes aceptar los terminos y condiciones',
          'error'
        );
        return;
      }

      this.login = false;
      if (seleccion === 'celular') {
        this.celular = true;
      }
      if (seleccion === 'correo') {
        this.correo = true;
      }
    }
    // Fin Método visualizar los pasos para iniciar session.

    // Método para aceptar los terminos y condiciones.
    aceptarTerminosCondiciones(seleccionado) {
      this.aceptarTerminos = seleccionado;
    }

    // Metodos opción celular
  validarNumeroCelular() {
    this.ngxService.start();
    this.numero_celular = this.form_validar_celular.get('celular').value;
    this.http.post('enviar_codigo_sms_web_comercio?celular=' + this.numero_celular, [], true).subscribe(
      response => {
          if (!response.error) {
            this.celular = false;
            this.verificar_codigo_celular = true;
            this.validacionTiempo();
            this.funciones.toast().fire({
              icon: 'success',
              title: 'Codigo enviado'
            });
            this.ngxService.stop();
          } else {

          }
      }, error => {
          this.funciones.toast().fire({
            icon: 'warning',
            title: 'Reintentando...'
        });
      });
  }

  validacionTiempo() {
    this.tiempoReenvio().subscribe(
      resp => {
        if (this.tiempo_reenvio > 0) {
          this.tiempo_reenvio--;
          this.validacionTiempo();
        }

        if (this.tiempo_reenvio === 0) {
          this.tiempo_reenvio = -1;
        }
      },
      err => {
      }
  );
  }

  tiempoReenvio() {
    return Observable.create(observer => {
            setTimeout(() => {
                observer.next(false);
            }, 5000);
        });
  }

  validarUsuario (event: any, modalLogin, modalDireccion, modalCompletarUsuario) {
    if (event.target.value.length === 4) {
      this.ngxService.start();
      let ruta: string;
      let asignarUsuario = true;
      let codigo = null;
      if (!this.correo && this.verificar_codigo_correo) {
        asignarUsuario = false;
        codigo = this.form_validar_correo.get('codigo').value;
        ruta = 'chequear_codigo_email_web_comercio?usuario_id=' + this.form_validar_correo.get('usuario_id').value + '&codigo=' + codigo;
      } else {
        codigo = this.form_validar_celular.get('codigo').value;
        ruta = 'verificar_codigo_sms_web_comercio?celular=' + this.numero_celular + '&codigo=' + codigo;
    }

    this.http.post(ruta, [], true).subscribe(
      response => {
          if (!response.error) {
            const credenciales = response.message;
            localStorage.setItem('apikey', credenciales.apikey);
            localStorage.setItem('apilogin', credenciales.apilogin);
            modalLogin.close();
            this.obtenerUsuario(modalDireccion, modalCompletarUsuario);
            this.funciones.toast().fire({
              icon: 'success',
              title: 'Bienvenido'
            });
          } else {
            this.funciones.toast().fire({
              icon: 'warning',
              title: 'Error validando el codigo, intenta nuevamente.'
            });
            this.ngxService.stop();
          }
      }, error => {
          this.funciones.toast().fire({
            icon: 'warning',
            title: 'Error validando el codigo, intenta nuevamente.'
        });
        this.ngxService.stop();
      });
  }
  }
  // Fin Metodo Celular


  // Método para obtner los datos despues de loguado un usuario
  obtenerUsuario(modalDireccion, modalCompletarUsuario) {
    const apiKey = localStorage.getItem('apikey');
    const apiLogin = localStorage.getItem('apilogin');
    // this.http.get('usuario?apiKey=' + apiKey + '&apiLogin=' + apiLogin, true).subscribe(
      this.http.get('usuario', true).subscribe(
      response => {
        this.usuario = response.message;
        if (this.usuario.nombre === '' || this.usuario.nombre === null) {
          this.modalService.open(modalCompletarUsuario, {windowClass: 'xlModalMediano', centered: true, size: 'lg'});
        } else {
          localStorage.setItem('usuario', JSON.stringify(response.message));
          this.obtenerDirecciones();
          this.modalService.open(modalDireccion, {windowClass: 'xlModalMediano', centered: true, size: 'lg'});
        }
        this.ngxService.stop();
      }, err => {
        this.ngxService.stop();
      });
  }
  // Fin Método para obtner los datos despues de loguado un usuario

  // Método para obtener las direcciones de un usuario logueado
  obtenerDirecciones(pararStop = true) {
    const apiKey = localStorage.getItem('apikey');
    const apiLogin = localStorage.getItem('apilogin');
    this.http.get('direcciones?apiKey=' + apiKey + '&apiLogin=' + apiLogin, true).subscribe(
      response => {
        if (!response.error) {
          localStorage.setItem('direcciones', JSON.stringify(response.message));
          this.direcciones = response.message;
          if (pararStop) {
            this.ngxService.stop();
          }
        } else {
          this.ngxService.stop();

          this.funciones.toast().fire({
            icon: 'warning',
            title: response.message
          });
          this.cerrarSession();
        }
      }, err => {
        this.ngxService.stop();
      });
  }
  // Fin Método para obtener las direcciones de un usuario logueado

  // Metodo para validar un correo
  validarCorreo() {

    this.correo_electronico = this.form_validar_correo.get('correo').value;
    if (this.correo) {
      this.ngxService.start();
      this.http.post('enviar_codigo_email_web_comercio?correo=' + this.correo_electronico, [], true).subscribe(
        response => {
            if (!response.error) {
              this.correo = false;
              this.verificar_codigo_correo = true;
              this.form_validar_correo.get('usuario_id').setValue(response.usuario_id);
              this.validacionTiempo();
              this.funciones.toast().fire({
                icon: 'success',
                title: 'Codigo enviado'
              });
              this.ngxService.stop();
            } else {
              this.funciones.toast().fire({
                icon: 'warning',
                title: 'Reintentando...'
              });
            this.ngxService.stop();
            }
        }, error => {
            this.funciones.toast().fire({
              icon: 'warning',
              title: 'Reintentando...'
            });
          this.ngxService.stop();
        });
    }
  }
  // Fin Metodo para validar un correo

  // Metodo para seleccionar una dirección
  seleccionarDireccion(seleccion: any, modal) {
    this.ngxService.start();
    this.consultarServiciosLatLng(seleccion.latitud, seleccion.longitud, seleccion, modal);
  }
  // Fin metodo para seleccionar una dirección


  // Metodo para consultar el menú de una dirección
  consultarServiciosLatLng(lat, lng, seleccion = null, modal?) {
    this.http.get('servicios?latitud=' + lat + '&longitud=' + lng, true).subscribe(
      response => {
          if (!response.error) {
            // Setear los servicios.
            this.funcionParaEmitir.emit(response.message);
            // console.log('entre');
            const orden = JSON.parse(localStorage.getItem('orden'));
            if (seleccion != null) {
              this.asignarDireccion(orden, seleccion);
            }
            localStorage.setItem('orden', JSON.stringify(orden));
            modal?.close();
        } else {
          this.funciones.swalDosBotones(
            'Ups',
            'Error, sin cobertura en la ubicación seleccionada',
            'question',
            'Reintentar',
            'No'
          ).then(
              result => {
                  if (result.value) {
                    this.modalService.open(modal, {centered: true, size: 'lg'});
                  }
              }
          );
        }
          this.ngxService.stop();
      }, err => {
            this.ngxService.stop();
      });
  }
  consultarMenuLatLng(lat, lng, seleccion = null, modal?) {
    this.http.get('menu_sede_direccion?proveedor=' + this.proveedorId + '&servicio_id=1&latitud=' + lat + '&longitud=' + lng, true).subscribe(
      response => {
          if (!response.error) {
            const comercio = response.message.comercio;
            const orden = JSON.parse(localStorage.getItem('orden'));
            if (seleccion != null) {
              this.asignarDireccion(orden, seleccion);
            }

            this.comercioCerrado = comercio.abierto;

            this.horarioComercioInicio = '2020-09-01 ' + comercio.hora_inicio.split('-')[0];
            this.horarioComercioFin = '2020-09-01 ' + comercio.hora_fin.split('-')[0];

            orden.comercio = {
              comentarios: comercio.comentarios,
              descripcion: comercio.descripcion,
              direccion: comercio.direccion,
              fecha_modificacion: comercio.fecha_modificacion,
              id: comercio.id,
              latitud: comercio.latitud,
              longitud: comercio.longitud,
              marketplace: comercio.marketplace,
              nombre: comercio.nombre,
              sede_id: comercio.sede_id,
              takeOut: false,
              valor_domicilio: comercio.valor_domicilio
            };

            localStorage.setItem('orden', JSON.stringify(orden));
            modal?.close();
        } else {
          this.funciones.swalDosBotones(
            'Ups',
            'Error, sin cobertura en la ubicación seleccionada',
            'question',
            'Reintentar',
            'No'
          ).then(
              result => {
                  if (result.value) {
                    this.modalService.open(modal, {centered: true, size: 'lg'});
                  }
              }
          );
        }

        this.ngxService.stop();
      }, err => {
            this.ngxService.stop();
      });
  }
  // Fin Metodo para consultar el menú de una dirección

  // Método para asignar una dirección
  asignarDireccion(orden, seleccion) {
    console.log(orden);
    this.direccionSeleccionada[0] = seleccion;
    orden.direccion = seleccion;
    orden.celular = this.usuario.celular;
    localStorage.setItem('direccion_seleccionada', JSON.stringify(this.direccionSeleccionada[0]));
  }
  // Fin Método para asignar una dirección

  // Método para eliminar una dirección
  eliminarDireccion(seleccion: any) {

  }
  // Fin Método para eliminar una dirección

  // Método para volver atras direcciones
  volverAtrasDirecciones(modal) {
    this.modalService.dismissAll();
    this.modalService.open(modal, {windowClass: 'xlModalMediano', centered: true, size: 'lg'});
  }
  // Fin Método para volver atras direcciones


  // Método para geolozalir una dirección en el mapa
  gelocalizarDireccion(modalMap, modalDireccion) {
    const direccion = this.form_creacion_direccion.get('direccion').value;
    const ciudad = this.form_creacion_direccion.get('ciudad').value[0].id;

    let formData: any = new FormData();
    formData.append('direccion', direccion);
    formData.append('ciudad', ciudad);

    // console.log(formData);
    // this.http.post('geolocalizar?ciudad=' + ciudad + '&direccion=' + direccion, [], true).subscribe(
      this.http.post('geolocalizar', formData, true).subscribe(
      response => {
          if (!response.error) {
            this.latitud = response.message.latitud;
            this.longitud = response.message.longitud;

            this.form_creacion_direccion.get('latitud').setValue(this.latitud);
            this.form_creacion_direccion.get('longitud').setValue(this.longitud);

            this.coordenadas = '(' + this.latitud + ',' + this.longitud + ')';
            modalDireccion.close();
            this.modalService.open(modalMap, {centered: true, size: 'lg'});
          } else {
              this.funciones.mostrarSwal(
                'Ups, no encontramos tú dirección',
                'Ups, tenemos problemas, por favor refresca la pagina!',
                'warning');
          }
      }, error => {
            this.funciones.mostrarCargando('Ups, tenemos problemas, por favor refresca la pagina!');
      });
      // Fin Método para geolozalir una dirección en el mapa
  }

  // Método para actualizar las notificaciones del usuario
  actualizarNotificacion(evento, seleccion) {

  }
  // Fin Método para actualizar las notificaciones del usuario


  // Método actualizar usuario
  actualizarUsuario(modalDirecciones, modalCompletarUsuario) {

    const apiKey = localStorage.getItem('apikey');
    const apiLogin = localStorage.getItem('apilogin');

    const nombre = this.form_creacion_usuario.get('nombre').value;
    const apellido = this.form_creacion_usuario.get('apellido').value;
    const correo = this.form_creacion_usuario.get('correo').value;


    let formData: any = new FormData();
      formData.append('apiLogin', apiLogin);
      formData.append('apiKey', apiKey);
      formData.append('nombre', nombre);
      formData.append('apellido', apellido);
      formData.append('correo', correo);

    this.http.post('complete_perfil', formData, true).subscribe(
      response => {
          if (!response.error) {
            this.usuario.nombre = nombre;
            this.usuario.apellido = apellido;
            this.usuario.correo = correo;
            localStorage.setItem('usuario', JSON.stringify(this.usuario));
            modalCompletarUsuario.close();
            this.obtenerDirecciones();
            this.modalService.open(modalDirecciones, {windowClass: 'xlModalMediano', centered: true, size: 'lg'});

            this.funciones.toast().fire({
              icon: 'success',
              title: 'Usuario actualizado correctamente!'
            });
          } else {
            this.funciones.toast().fire({
              icon: 'warning',
              title: 'Error actualizando datos'
          });
          }
      }, error => {
          this.funciones.toast().fire({
            icon: 'warning',
            title: 'Reintentando...'
        });
      });
  }
  // Fin Método actualizar usuario

  // Método para ver las ordenes actuales
  verOrdenActual(modal, orden?) {

  }
  // Fin Método para ver las ordenes actuales

  // Método para crear una orden
  crearOrden() {
    this.carritoCompras = {celular: '', comentario: '', comercio: {
                          comentarios: true,
                          descripcion: '',
                          direccion: '',
                          fecha_modificacion: '',
                          id: '',
                          latitud: '',
                          longitud: '',
                          marketplace: false,
                          nombre: '',
                          sede_id: '',
                          takeOut: false,
                          valor_domicilio: ''
                      }, direccion: {
                                    direccion: '',
                                    descripcion: '',
                                    id: '',
                                    latitud: '',
                                    longitud: '',
                                    nombre: ''
                      },
                      estado_entrega: 0,
                      fecha_programacion: '',
                      id: 0,
                      marketplace: false,
                      pago: 1,
                      productos: [],
                      propina : 0,
                      servicio_id : 1,
                      take_out : false,
                      total: 0,
                      tu_pago : 0,
                      sistema: {
                      app: {
                            builType : 'release',
                            version : 0,
                      },
                      os: {
                            name: '',
                            model: '',
                            sdk: '',
                            version: ''
                      }
                      }
                    };

      localStorage.setItem('orden', JSON.stringify(this.carritoCompras));
  }
  // Fin Método para crear una orden

  volverInicio() {
    localStorage.removeItem('nombre_proveedor');
    localStorage.removeItem('id_proveedor');
    if (this.router.url === '/servicio') {
      localStorage.removeItem('visualizar');
      this.visualizarEmit.emit('servicio');
      this.visualizarServicios = true;
    }
    this.router.navigate(['/']);
    const direccionSeleccionada = JSON.parse(localStorage.getItem('direccion_seleccionada'));
    this.consultarServiciosLatLng(direccionSeleccionada.latitud, direccionSeleccionada.longitud);
  }

  visualizar() {
    const visualizar = JSON.parse(localStorage.getItem('visualizar'));
    if (visualizar !== null) {
      this.visualizarServicios = visualizar.visualizar;
    }
  }

  agregarDireccion(dato, modalMap, modalDireccion) {
    modalMap.close();
    if (dato.hacer) {
      const apiKey = localStorage.getItem('apikey');
      const apiLogin = localStorage.getItem('apilogin');

      const direccion = this.form_creacion_direccion.get('direccion').value;
      const ciudad = this.form_creacion_direccion.get('ciudad').value[0].id;
      const nombre = this.form_creacion_direccion.get('nombre').value;
      const discripcion = this.form_creacion_direccion.get('nombre').value;
      const lat = this.form_creacion_direccion.get('latitud').value;
      const lng = this.form_creacion_direccion.get('longitud').value;

      let formData: any = new FormData();
      formData.append('apiLogin', apiLogin);
      formData.append('apiKey', apiKey);
      formData.append('direccion', direccion);
      formData.append('nombre', nombre);
      formData.append('ciudad', ciudad);
      formData.append('descripcion', discripcion);
      formData.append('latitud', lat);
      formData.append('longitud', lng);

      this.http.post('agregar_direccion', formData, true).subscribe(
        response => {
            if (!response.error) {
              this.direcciones.unshift(response.message);
              this.direccionSeleccionada[0] = response.message;
              this.consultarMenu(modalMap);
            } else {
              this.funciones.mostrarCargando('Ups, tenemos problemas, por favor refresca la pagina!');
            }
        }, error => {
              this.funciones.mostrarCargando('Ups, tenemos problemas, por favor refresca la pagina!');
        });
    } else {
      this.modalService.open(modalDireccion, {windowClass: 'xlModalMediano', centered: true, size: 'lg'});
    }
  }

  consultarMenu(modal) {
    const apiKey = localStorage.getItem('apikey');
    const apiLogin = localStorage.getItem('apilogin');
    const lat = this.form_creacion_direccion.get('latitud').value;
    const lng = this.form_creacion_direccion.get('longitud').value;

    this.consultarServiciosLatLng(lat, lng, null, modal);
  }
}
