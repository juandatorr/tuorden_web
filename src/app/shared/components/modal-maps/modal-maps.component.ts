import {AfterViewInit, Component, ElementRef, EventEmitter, Input, NgZone, OnInit, Output, ViewChild, ViewEncapsulation} from '@angular/core';
import {NgbCalendar, NgbDate, NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';
import {MapsAPILoader, MouseEvent} from '@agm/core';
// @ts-ignore
import {} from 'googlemaps';
import { ConexionLaravelHttpService } from '../../services/conexion-laravel-http.service';
import { FuncionesComunesService } from '../../services/funciones-comunes.service';

@Component({
    selector: 'app-modal-maps',
    templateUrl: './modal-maps.component.html',
    styleUrls: ['./modal-maps.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ModalMapsComponent implements OnInit, AfterViewInit {
    // @Input() type; 
    // @Input() urlConsultaFiltros;
    @Input() titulo;
    @Input() poligono;
    @Input() botonAccion;
    // @Input() complemento;
    @Input() dato;
    @Input() botonCerrar;
    @Output() accionRealizada: EventEmitter<any> = new EventEmitter<any>();

    datosMaps: any = {};
    direccionInput = '';

    @ViewChild('buscar', {static: false})
    public searchElementRef: ElementRef;

    pointList: { lat: number; lng: number }[] = [];
    selectedArea = 0;
    drawingManager: any;
    mapGlobal;
    misCoordenadas = [];

    constructor(private mapsAPILoader: MapsAPILoader,
                private ngZone: NgZone,
                private http: ConexionLaravelHttpService,
                public calendar: NgbCalendar,
                public funciones: FuncionesComunesService,
                public formatter: NgbDateParserFormatter) {
    }

    options: any = {
        lat: 33.5362475,
        lng: -111.9267386,
        zoom: 10,
        fillColor: '#DC143C',
        draggable: true,
        editable: true,
        visible: true
    };

    private setCurrentLocation() {
        if ('geolocation' in navigator) {
            navigator.geolocation.getCurrentPosition((position) => {
                //this.datosMaps.latitude = position.coords.latitude;
                //this.datosMaps.longitude = position.coords.longitude;
                //console.log(this.datosMaps.latitude, this.datosMaps.longitude);
                /* this.datosMaps.zoom = 15;
                this.getAddress(this.datosMaps.latitude, this.datosMaps.longitude); */
                this.datosMaps.latitude = this.dato.get('latitud').value * 1;
                this.datosMaps.longitude = this.dato.get('longitud').value * 1;
                this.datosMaps.zoom = 15;
                //this.getAddress(this.datosMaps.latitude, this.datosMaps.longitude);
            });
        }
    }

    markerDragEnd(event: MouseEvent) {
        this.datosMaps.latitude = event.coords.lat;
        this.datosMaps.longitude = event.coords.lng;
        this.getAddress(this.datosMaps.latitude, this.datosMaps.longitude);
    }

    getAddress(latitude, longitude) {
        this.datosMaps.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
            if (status === 'OK') {
                if (results[0]) {
                    this.datosMaps.zoom = 15;
                    this.datosMaps.address = results[0].formatted_address;
                    this.direccionInput = results[0].formatted_address;
                } else {
                    window.alert('No results found');
                }
            } else {
                window.alert('Geocoder failed due to: ' + status);
            }

        });
    }

    ngOnInit() {
    }

    ngAfterViewInit() {
        this.mapsAPILoader.load().then(() => {
            //this.setCurrentLocation();
            this.datosMaps.latitude = this.dato.get('latitud').value * 1;
            this.datosMaps.longitude = this.dato.get('longitud').value * 1;
            this.datosMaps.zoom = 15;
        });
    }

    enviarDatos(hacer?) {
        this.datosMaps.hacer = hacer;
        this.accionRealizada.emit(this.datosMaps);
    }

    onMapReady(map) {
        this.mapGlobal = map;
            const cuadrante = this.dato;
            if (cuadrante !== null) {
                const cudrante1 = cuadrante.split('(');
                for (let i = 0; i < cudrante1.length; i++) {
                    if (cudrante1[i].length > 0) {
                        const posiciones = cudrante1[i].split(',');
                        let lat = 0;
                        let lng = 0;
                        for (let j = 0; j < posiciones.length; j++) {
                            if (Number(posiciones[j]) !== 0) {
                                lat = Number(posiciones[0]);
                                lng = Number(posiciones[1].split(')')[0]);
                            }
                        }
                        this.pointList.push({lat: lat, lng: lng});
                        this.misCoordenadas.push(new google.maps.LatLng(lat, lng));
                    }
                }

                this.convertirCoordenadasAString();
                const bounds = new google.maps.LatLngBounds();

                for (let i = 0; i < this.misCoordenadas.length; i++) {
                    bounds.extend(this.misCoordenadas[i]);
                }
                const polyOptions = {
                    drawingControl: true,
                    path: this.misCoordenadas,
                    draggable: true,
                    editable: true,
                    strokeColor: '#FF0000',
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: '#0000FF',
                    fillOpacity: 0.6,
                    drawingMode: google.maps.drawing.OverlayType.POLYGON
                };


                this.drawingManager = new google.maps.Polygon(polyOptions);
                this.drawingManager.setMap(map);

                google.maps.event.addListener( this.drawingManager.getPath(), 'set_at', () => {
                    console.log('entre set_at');
                    this.updatePointList(this.drawingManager.getPath());
                });

                google.maps.event.addListener( this.drawingManager.getPath(), 'insert_at', () => {
                    console.log('entre insert_at');
                    this.updatePointList(this.drawingManager.getPath());
                });

                google.maps.event.addListener( this.drawingManager.getPath(), 'remove_at', () => {
                    console.log('entre remove_at');
                    this.updatePointList(this.drawingManager.getPath());
                });

            } else {
                this.initDrawingManager(map);
            }

    }

    initDrawingManager = (map: any) => {
        const opti = {
            drawingControl: true,
            polygonOptions: {
                draggable: true,
                editable: true
            },
            drawingMode: google.maps.drawing.OverlayType.POLYGON
        };

        this.drawingManager = new google.maps.drawing.DrawingManager(opti);
        this.drawingManager.setMap(map);

        google.maps.event.addListener(
            this.drawingManager,
            'overlaycomplete',
            (event) => {
                if (event.type === google.maps.drawing.OverlayType.POLYGON)    {
                    const paths = event.overlay.getPaths();
                    for (let p = 0; p < paths.getLength(); p++) {
                        google.maps.event.addListener(
                            paths.getAt(p),
                            'set_at',
                            () => {
                                if (!event.overlay.drag) {
                                    this.updatePointList(event.overlay.getPath());
                                }
                            }
                        );
                        google.maps.event.addListener(
                            paths.getAt(p),
                            'insert_at',
                            () => {
                                this.updatePointList(event.overlay.getPath());
                            }
                        );
                        google.maps.event.addListener(
                            paths.getAt(p),
                            'remove_at',
                            () => {
                                this.updatePointList(event.overlay.getPath());
                            }
                        );
                    }
                    this.updatePointList(event.overlay.getPath());
                }
                if (event.type !== google.maps.drawing.OverlayType.MARKER) {
                    // Switch back to non-drawing mode after drawing a shape.
                    this.drawingManager.setDrawingMode(null);
                    // To hide:
                    this.drawingManager.setOptions({
                        drawingControl: false,
                    });
                }
            }
        );
    }

    updatePointList(path) {
        this.pointList = [];
        const len = path.getLength();
        for (let i = 0; i < len; i++) {
            this.pointList.push(
                path.getAt(i).toJSON()
            );
        }
        this.selectedArea = google.maps.geometry.spherical.computeArea(
            path
        );

        this.convertirCoordenadasAString();
    }

    convertirCoordenadasAString() {
        let stringCoordenadas = '';
        stringCoordenadas = '(';
        for (let i = 0; i < this.pointList.length; i++) {
            if (i > 0) {
                stringCoordenadas += ',';
            }
            stringCoordenadas += '(' + this.pointList[i].lat + ',' + this.pointList[i].lng + ')';
        }
        stringCoordenadas += ')';

        this.datosMaps.cuadrante = stringCoordenadas;
    }

}

