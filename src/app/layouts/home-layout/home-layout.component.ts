import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, Input, ViewEncapsulation, Output, Inject} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgbModal, NgbModalConfig, NgbPopover } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators } from '@angular/forms';
import { ConexionLaravelHttpService } from 'src/app/shared/services/conexion-laravel-http.service';
import { FuncionesComunesService } from 'src/app/shared/services/funciones-comunes.service';
import { Observable } from 'rxjs';
import { HomeComponent } from 'src/app/pages/home/home.component';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { EventEmitter } from 'events';
import { DOCUMENT } from '@angular/common';
import { MapsAPILoader } from '@agm/core';

@Component({
  selector: 'app-home-layout',
  templateUrl: './home-layout.component.html',
  styleUrls: ['./home-layout.component.scss'],
})
export class  HomeLayoutComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('homeDatos') homeDatos: HomeComponent;
  @ViewChild('carrito') carrito: NgbPopover;

  // Atributos Mapa
  sede_latitude;
  sede_longitude;

  picker_latitude;
  picker_longitude;

  orden_latitude;
  orden_longitude;

  zoom;
  timeOut;
  // Fin Atributos Mapa

  isCollapsed  = false;
  proveedorId = '';

  usuario = null;

  // Variable productos agregados
  mensajePopover;
  cantidadPopover;
  valorPopover;
  // Fin Variable productos agregados

  constructor(private router: Router,
              private http: ConexionLaravelHttpService,
              public modalService: NgbModal,
              public config: NgbModalConfig,
              public funciones: FuncionesComunesService,
              public fb: FormBuilder,
              private ngxService: NgxUiLoaderService,
              private mapsAPILoader: MapsAPILoader,
              @Inject(DOCUMENT) document: any,
              private rutaActiva: ActivatedRoute
              ) {
                config.backdrop = 'static';
                config.keyboard = false;
                if (localStorage.getItem('id_proveedor') === null) {
                  this.router.navigate(['/']);
                }

                this.proveedorId = localStorage.getItem('id_proveedor');

              document.getElementById('titulo_pagina').innerHTML = localStorage.getItem('nombre_proveedor') ? localStorage.getItem('nombre_proveedor') : 'Tu Orden';
              document.getElementById('img_fav').setAttribute('href', 'https://tuorden.com.co/web/image_proveedor/img-big-' + this.proveedorId + '.png');

              const usuarioJson = localStorage.getItem('usuario');

              if (usuarioJson) {
                this.usuario = JSON.parse(usuarioJson);
            }
          }



  ngOnInit() {
    const html = document.getElementsByTagName('html')[0];
    html.classList.add('auth-layout');
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('bg-default');
    this.router.events.subscribe((event) => {
      this.isCollapsed = true;
    });
  }

  ngOnDestroy() {
    const html = document.getElementsByTagName('html')[0];
    html.classList.remove('auth-layout');
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('bg-default');
  }

  ngAfterViewInit() {

  }

  verOrdenActual(modal, orden?) {

    this.ngxService.start();
    const prove = localStorage.getItem('id_proveedor');
    const serv = JSON.parse(localStorage.getItem('visualizar')).servicio.id;
    const nom = prove + serv;
    const compraJson = localStorage.getItem('compra' + nom);
    let compra;
    if (compraJson.length > 0) {
      compra = JSON.parse(compraJson);
    }
    if ( orden != null) {
      compra = orden;
    }
    this.homeDatos.obtenerInformacionOrden(compra.id, false, modal);

    this.mapsAPILoader.load().then(() => {
      this.sede_latitude = Number(this.homeDatos.compraOrden?.sede_latitud);
      this.sede_longitude = Number(this.homeDatos.compraOrden?.sede_longitud);

      this.picker_latitude = Number(this.homeDatos.compraOrden?.latitud_picker);
      this.picker_longitude = Number(this.homeDatos.compraOrden?.longitud_picker);

      this.orden_latitude = Number(this.homeDatos.compraOrden?.orden_latitud);
      this.orden_longitude = Number(this.homeDatos.compraOrden.orden_longitud);

      this.zoom = 10;
    });

    this.verificarOrden(true, modal);
  }

  verificarOrden(validar, modal?) {
    if (this.homeDatos.compraOrden === null) {
        modal.close();
        clearTimeout(this.timeOut);
    } else {
      this.cargarDatosOrden().subscribe(
        resp => {
          if  (validar) {
            const compra = JSON.parse(localStorage.getItem('compra'));
            this.homeDatos.obtenerInformacionOrden(compra.id, false);
            this.verificarOrden(validar);
          } else {
            validar = false;
            clearTimeout(this.timeOut);
          }
        },
        err => {
        }
      );
    }
  }

  cargarDatosOrden() {
    return Observable.create(observer => {
      this.timeOut = setTimeout(() => {
                observer.next(false);
            }, 10000);
        });
  }

  visualizarCarrito() {

    const direccionSeleccionada = JSON.parse(localStorage.getItem('direccion_seleccionada'));
    console.log(direccionSeleccionada);

    this.homeDatos.abrirToogle('carrito');
    this.homeDatos.setDireccion(direccionSeleccionada.direccion);
    this.homeDatos.setDatosCarrito();
  }

  mostrarProductoAgregado(respuesta, pop) {
    this.cantidadPopover = respuesta.cantidad;
    this.mensajePopover = respuesta.mensaje;
    this.valorPopover = respuesta.valor;
    pop.open();
  }
}
