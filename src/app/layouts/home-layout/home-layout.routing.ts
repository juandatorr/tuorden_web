import { Routes } from '@angular/router';

import { HomeComponent } from 'src/app/pages/home/home.component';
import { HomeLayoutComponent } from './home-layout.component';

export const HomeLayoutRoutes: Routes = [
    { path: '', component: HomeLayoutComponent},
    // { path: 'producto/:proveedor_id', component: HomeComponent},
];
