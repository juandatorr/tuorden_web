import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HomeComponent } from 'src/app/pages/home/home.component';
import {AngularMultiSelectModule} from 'angular2-multiselect-dropdown';
import {SidebarModule} from 'ng-sidebar';
import { UiSwitchModule } from 'ngx-ui-switch';
import { AgmCoreModule } from '@agm/core';
import { ServicioLayoutRoutes } from './servicio-layout.routing';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ServicioLayoutRoutes),
    FormsModule,
    AngularMultiSelectModule,
    ReactiveFormsModule,
    SidebarModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBt68PEyeET9JkgAGVqtPbVRXb6IXbU0E4',
      libraries: ['places', 'drawing','geometry']
    })
  ],
  declarations: []
})
export class ServicioLayoutModule { }
