import { Routes } from '@angular/router';
import { ServicioComponent } from 'src/app/pages/servicio/servicio.component';
import { ServicioLayoutComponent } from './servicio-layout.component';

export const ServicioLayoutRoutes: Routes = [
    { path: '', component: ServicioLayoutComponent, children: [
        {path: '', redirectTo: 'servicio', pathMatch: 'full'},
        {path: 'servicio', component: ServicioComponent},
    ]},
];
