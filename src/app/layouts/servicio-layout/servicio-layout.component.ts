import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, Input, ViewEncapsulation, Output, Inject, ElementRef, NgZone} from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalConfig, NgbPopover } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators } from '@angular/forms';
import { ConexionLaravelHttpService } from 'src/app/shared/services/conexion-laravel-http.service';
import { FuncionesComunesService } from 'src/app/shared/services/funciones-comunes.service';
import { Observable } from 'rxjs';
import { HomeComponent } from 'src/app/pages/home/home.component';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { EventEmitter } from 'events';
import { DOCUMENT } from '@angular/common';
import { MapsAPILoader } from '@agm/core';
import { ServicioComponent } from 'src/app/pages/servicio/servicio.component';
import { HeaderComponent } from 'src/app/shared/components/header/header.component';

@Component({
  selector: 'app-servicio-layout',
  templateUrl: './servicio-layout.component.html',
  styleUrls: ['./servicio-layout.component.scss'],
})
export class  ServicioLayoutComponent implements OnInit, OnDestroy {

  @ViewChild('servicio', {static: false})
  public servicio: ServicioComponent;

  @ViewChild('header', {static: false})
  public header: HeaderComponent;

  lat: number;
  lng: number;
  zoom = 1;
  private geoCoder;

  @ViewChild('search') public searchElementRef: ElementRef;

  public isCollapsed = true;


  constructor(private router: Router,
              private http: ConexionLaravelHttpService,
              public modalService: NgbModal,
              public config: NgbModalConfig,
              public funciones: FuncionesComunesService,
              public fb: FormBuilder,
              private ngxService: NgxUiLoaderService,
              private mapsAPILoader: MapsAPILoader,
              private ngZone: NgZone,
              @Inject(DOCUMENT) document: any,
              ) {
                config.backdrop = 'static';
                config.keyboard = false;

                document.getElementById('titulo_pagina').innerHTML = 'Tu Orden';
                document.getElementById('img_fav').setAttribute('href', 'https://tuorden.com.co/web/index/images/logo.png');

            }

  ngOnInit() {
    const html = document.getElementsByTagName('html')[0];
    html.classList.add('auth-layout');
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('bg-default');
    this.router.events.subscribe((event) => {
      this.isCollapsed = true;
    });

    /*this.mapsAPILoader.load().then(() => {
      this.geoCoder = new google.maps.Geocoder;

      const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          const place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          this.lat = place.geometry.location.lat();
          this.lng = place.geometry.location.lng();
          this.zoom = 12;
        });
      });
    });*/
  }

  ngOnDestroy() {
    const html = document.getElementsByTagName('html')[0];
    html.classList.remove('auth-layout');
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('bg-default');
  }

  setearInformacion(evento) {
    this.servicio.cargarServicios(evento);
  }

  visualizar(componente) {
    this[componente].visualizar();
  }

  cerrarSession() {
    this.header.cerrarSession();
  }
}
