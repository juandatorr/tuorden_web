import { Component, OnInit, Output, EventEmitter, ViewEncapsulation, Inject, ViewChild, TemplateRef } from '@angular/core';

// core components
import { ConexionLaravelHttpService } from 'src/app/shared/services/conexion-laravel-http.service';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Validators, FormBuilder } from '@angular/forms';
import { FuncionesComunesService } from 'src/app/shared/services/funciones-comunes.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { HttpHeaders } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import { MapsAPILoader } from '@agm/core';
import { log } from 'console';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {


  @ViewChild('modalPromocion', {static: true}) modalPromocion: TemplateRef<any>;

  @Output() carrito: EventEmitter<any> = new EventEmitter<any>();
  @Output() login: EventEmitter<any> = new EventEmitter<any>();
  @Output() direccionEmit: EventEmitter<any> = new EventEmitter<any>();
  @Output() setProveedor: EventEmitter<any> = new EventEmitter<any>();

  // Variables SIDEBAR
  public _opened: Boolean = false;
  public _modeNum = 0;
  public _positionNum = 0;
  public _dock: Boolean = false;
  public _closeOnClickOutside: Boolean = false;
  public _closeOnClickBackdrop: Boolean = false;
  public _showBackdrop: Boolean = false;
  public _animate: Boolean = true;
  public _trapFocus: Boolean = true;
  public _autoFocus: Boolean = true;
  public _keyClose: Boolean = false;
  public _autoCollapseHeight = null;
  public _autoCollapseWidth = null;
  public _MODES: Array<string> = ['over', 'slide', 'push'];
  public _POSITIONS: Array<string> = ['right', 'left', 'top', 'bottom'];

  collapedSideBar: boolean;
  // Fin Variables SIDEBAR

  categorias = [];
  categoriasCompletas = [];

  carritoCompras: any = {celular: '', comentario: '', comercio: {
                                                                comentarios: true,
                                                                descripcion: '',
                                                                direccion: '',
                                                                fecha_modificacion: '',
                                                                id: '',
                                                                latitud: '',
                                                                longitud: '',
                                                                marketplace: false,
                                                                nombre: '',
                                                                sede_id: '',
                                                                takeOut: false,
                                                                valor_domicilio: ''
                                        }, direccion: {
                                                        direccion: '',
                                                        descripcion: '',
                                                        id: '',
                                                        latitud: '',
                                                        longitud: '',
                                                        nombre: ''
                                        },
                                        estado_entrega: 0,
                                        fecha_programacion: '',
                                        id: 0,
                                        marketplace: false,
                                        pago: 1,
                                        productos: [],
                                        propina : 0,
                                        servicio_id : 1,
                                        take_out : false,
                                        total: 0,
                                        tu_pago : 0,
                                        sistema: {
                                        app: {
                                              builType : 'release',
                                              version : 0,
                                        },
                                        os: {
                                              name: '',
                                              model: '',
                                              sdk: '',
                                              version: ''
                                          }
                                        }
  };
  valorTemporalProducto;

  productoSeleccionado;

  configuracionSelect = {};

  extrasSeleccionados = [];
  extrasSeleccionadosOpcional = [];

  direccion;

  // Carrito de compras
  validarCarrito = true;
  continuarCarrito = false;

  celular;
  tiempo_entrega;

  medios_pago;
  propina = 1000;
  total_productos;
  total_domicilios;
  total_tu_pago;
  tu_pago = 0;
  total_a_pagar;
  tuPagoSeleccion = false;

  // Atributos Tarjetas
  form_creacion_tarjeta  = this.fb.group({
    tipo_documento : ['', Validators.required],
    documento: ['', Validators.required],
    numero : ['', Validators.required],
    fecha_expiracion : ['', [Validators.required, Validators.maxLength(7)]],
    cvv : ['', Validators.required],
    nombre : ['', Validators.required],
    apellido : ['', Validators.required],
    correo : ['', Validators.required],
  });

  tipo_documentos = [{id: 1, nombre: 'Cédula'}, {id: 2, nombre: 'Cédula Extranjeria'}, {id: 3, nombre: 'Tarjeta Identidad'}, {id: 4, nombre: 'Pasaporte'}];
  tarjetasUsuario = [];
  tarjetaUsuarioSelaccionada;
  medio = 0;
  // Fin Atributos Tarjetas

  cantidadCarrito = 0;

  compraOrden: any = null;
  proveedorId;
  servicioId;

  disabledAgregarCarrito = true;

  usuario = null;

  localNombre = '';

  // Atributos buscar productos
  busquedaProductos = '';

  constructor(private http: ConexionLaravelHttpService,
              public config: NgbModalConfig,
              public fb: FormBuilder,
              public funciones: FuncionesComunesService,
              public modalService: NgbModal,
              private ngxService: NgxUiLoaderService,
              @Inject(DOCUMENT) document: any,
              private rutaActiva: ActivatedRoute,
              private router: Router
              ) {
                  // this.cargarDominio(document.location.hostname);
                  this.ngxService.start();
                  if (localStorage.getItem('id_proveedor') === null) {
                    this.ngxService.stop();
                    this.router.navigate(['/']);
                  } else {
                    this.proveedorId = localStorage.getItem('id_proveedor');
                    this.servicioId = JSON.parse(localStorage.getItem('visualizar')).servicio.id;

                    this.localNombre = this.proveedorId + '' + this.servicioId;
                    const direccionJsonSeleccionada = localStorage.getItem('direccion_seleccionada');
                    if (direccionJsonSeleccionada != null) {
                      this.cargarProductosComercio();
                    }
                    const carritoJson = localStorage.getItem(this.localNombre);

                    if (carritoJson) {
                      this.carritoCompras = JSON.parse(carritoJson);
                    }

                    // this.cargarProductosComercio();
                    /* const usuarioJson = localStorage.getItem('usuario');

                    if (usuarioJson) {
                        this.usuario = JSON.parse(usuarioJson);
                    } */

                    this.usuario = this.obtenerUsuario();

                    const cantidad = localStorage.getItem('cantidad_carritos' + this.localNombre);
                    if (cantidad) {
                      this.cantidadCarrito = Number(cantidad);
                    }

                    const compra = localStorage.getItem('compra' + this.localNombre);
                    if (compra) {
                      this.compraOrden = JSON.parse(compra);
                    }
                    //this.ngxService.stop();
                    this.cargarSelect();
                  }
              }

  public isCollapsed = false;

  ngOnInit() {

  }


  cargarSelect() {
    this.configuracionSelect = {
      singleSelection: true,
      text: 'Selecciona una opción',
      classes: 'myclass',
      primaryKey: 'id',
      labelKey: 'nombre',
      noDataLabel: 'No se encontraron datos',
      enableSearchFilter: true,
      enableFilterSelectAll: false,
      searchPlaceholderText: 'Buscar',
      // lazyLoading: true,
    };
  }

  cargarProductosComercio() {
    this.ngxService.start();
    this.http.get('menu_sede?proveedor=' + this.proveedorId, true).subscribe(
      response => {
        this.cargarProducto(response.message);
      }, err => {
      }
    );
  }

  cargarProducto(categorias) {
    this.categorias = categorias;
    const categoriaTodos = {'id': 0, 'nombre': 'Todos', 'fecha_modificacion': null, 'productos': []};
    let promocionServicio = this.carritoCompras.comercio.promocion !== undefined ? this.carritoCompras.comercio.promocion : {id: 0}; // promoción que viene desde la vista de servicio
    this.categorias.unshift(categoriaTodos);
    this.categoriasCompletas = this.categorias;
    for (let i = 0; i < this.categoriasCompletas.length; i++) {
      const productos = this.categoriasCompletas[i].productos;
      for (let j = 0; j < productos.length; j++) {
        const producto = productos[j];
        if ( producto.valor === '0') {
          producto.sin_valor = true;
          producto.valor = producto.grupo_extras[0].extras.filter( x => x.valor !== 0)[0]?.valor;
          producto.valor_temporal = producto.grupo_extras.filter( x => x.valor !== 0)[0]?.extras[0].valor;

          if ( producto.valor === undefined ) {
            producto.valor = producto.grupo_extras[1].extras.filter( x => x.valor !== 0)[0]?.valor;
          }

          if ( producto.valor_temporal === undefined) {
            producto.valor_temporal = producto.grupo_extras.filter( x => x.valor !== 0)[0]?.extras[1].valor;
          }
        } else {
          producto.sin_valor = false;
          producto.valor = producto.valor;
          producto.valor_temporal = producto.valor;
        }
        this.validarDescuento(producto);
        if (promocionServicio.id === producto.id) {
          promocionServicio = producto;
        }
      }
      this.categoriasCompletas[i].productosCompletos = productos;
      this.ngxService.stop();
    }

    // aqui va el cargar el producto en el modal;
    if (promocionServicio.id !== 0) {
      this.abrirModales(this.modalPromocion, promocionServicio);
    }

  }

  validarDescuento(producto) {
    const diferencia = Number(producto.valor_anterior) - Number(producto.valor);
    // console.log(diferencia);
    if (diferencia > 0) {
      const porcentaje = diferencia / producto.valor_anterior;
      const redondeo = this.redondear(porcentaje, 1);
      producto.descuento = redondeo * 100;
    } else {
      producto.descuento = 0;
    }
  }

  redondear(num, dec) {
    const exp = Math.pow(10, dec || 2); // 2 decimales por defecto
    return parseInt('' + num * exp, 10) / exp;
  }
  // Abrir Modal
  abrirModales(modal, producto) {
    this.disabledAgregarCarrito = true;
    this.extrasSeleccionados = [];
    // producto.valor_temporal = producto.valor;
    this.productoSeleccionado = producto;
    this.productoSeleccionado.cantidad = 1;

    this.productoSeleccionado.total = (Number(this.productoSeleccionado.cantidad) * Number(this.productoSeleccionado.valor_temporal));

    for (let i = 0; i < this.productoSeleccionado.grupo_extras.length; i++) {
         const gruopoExtra =  this.productoSeleccionado.grupo_extras[i];
          if (gruopoExtra.extras.length === 1) {
              this.extrasSeleccionados[i] = gruopoExtra.extras[0];
              this.extrasSeleccionados[i].grupo_extras = this.productoSeleccionado.grupo_extras;
              gruopoExtra.noSeleccionado = false;
              this.setearGrupoExtra(gruopoExtra, this.extrasSeleccionados[i]);
          } else {
            this.extrasSeleccionados[i] = [];
            gruopoExtra.noSeleccionado = true;
            if (gruopoExtra.tipo === 2) {
              for (let j = 0; j < gruopoExtra.extras.length; j++) {
                this.extrasSeleccionados[i][j] = gruopoExtra.extras[j];
              }
            }
          }
    }
    this.modalService.open(modal, {centered: true, size: 'lg'});
  }

  abrirModal(modalAbrir, modalCerrar, seleccion) {
      if (seleccion === 'tarjeta') {
        modalCerrar.dismiss();
        this.modalService.open(modalAbrir, {centered: true, size: 'lg'});
      }
  }

  cerrarToogle() {
    this._opened = false;
  }

  mostrarProductos(categoria) {
    this.categoriasCompletas = [];
    if (categoria.id === 0)  {
      this.categoriasCompletas = this.categorias;
    } else {
      this.categoriasCompletas[0] = categoria;
    }
  }

  restarCantidad(seleccionado = true, producto = null) {
    if (seleccionado) {
      if (this.productoSeleccionado.cantidad === 1) {
        this.productoSeleccionado.cantidad = 1;
      } else {
        this.productoSeleccionado.cantidad--;
        this.productoSeleccionado.total = (Number(this.productoSeleccionado.cantidad) * Number(this.productoSeleccionado.valor_temporal));
      }
    } else {
      if (producto.cantidad === 1) {
        producto.quitar = true;
        producto.cantidad = 1;
        producto.total = (Number(producto.cantidad) * Number(producto.valor_temporal));
      } else {
        producto.quitar = false;
        producto.cantidad--;
        producto.total = (Number(producto.cantidad) * Number(producto.valor_temporal));
      }
    }

    this.totalizarProductos();
  }

  sumarCantidad(seleccionado = true, producto = null) {
    if  (seleccionado) {
      this.productoSeleccionado.cantidad++;
      this.productoSeleccionado.total = (Number(this.productoSeleccionado.cantidad) * Number(this.productoSeleccionado.valor_temporal));
    } else {
      producto.quitar = false;
      producto.cantidad++;
      producto.total = (Number(producto.cantidad) * Number(producto.valor_temporal));
    }

    this.totalizarProductos();
  }

  eliminarProductoCarrito(productoEliminar) {
    this.carritoCompras.productos.forEach( (producto, index) => {
        if (producto.id === productoEliminar.id) {
              this.carritoCompras.productos.splice(index, 1);
        }
    });

    this.totalizarProductos();
  }

  totalizarProductos() {
    this.cantidadCarrito = 0;
    let total_orden = 0;

    this.carritoCompras.productos.forEach(producto => {
      this.cantidadCarrito += Number(producto.cantidad);
      localStorage.setItem('cantidad_carritos' + this.localNombre, this.cantidadCarrito.toString());
    });

    total_orden = this.carritoCompras.productos.reduce((a, b) => a + b.total, 0);
    this.carritoCompras.total = total_orden;
    localStorage.setItem(this.localNombre, JSON.stringify(this.carritoCompras));
  }

  agregarProductoCarrito(modal) {
    this.cantidadCarrito = 0;
    const productosCarrito = this.carritoCompras.productos;
    const producto = this.productoSeleccionado;
    let total_orden = 0;
    let insertar = true;
    let contador = 0;

    const extrasSeleccionadosProducto = [];

    // Extras Seleccionados
    for (let i = 0; i < this.extrasSeleccionados.length; i++) {
      const extraSeleccionado = this.extrasSeleccionados[i];
      if (extraSeleccionado.grupo_extra !== undefined) {
        extrasSeleccionadosProducto.push({'id' : extraSeleccionado.id,
                          'id_extra_proveedor': 0,
                          'nombre': extraSeleccionado.nombre,
                          'valor': extraSeleccionado.valor});
      } else {
        for (let j = 0; j < extraSeleccionado.length; j++) {
            const extra = extraSeleccionado[j];
            if (extra.seleccionado) {
              extrasSeleccionadosProducto.push({'id' : extra.id,
                          'id_extra_proveedor': 0,
                          'nombre': extra.nombre,
                          'valor': extra.valor});
            }
        }
      }
    }
    // Fin Extras Seleccionados

    for (const productoCarrito of productosCarrito) {
        contador = 0;
        const extrasExistentesTemporal =  JSON.parse(JSON.stringify(productoCarrito.extras));
        const extraSeleccionadosTemporal = JSON.parse(JSON.stringify(extrasSeleccionadosProducto));

        if (producto.id === productoCarrito.id) {
          for (let i = 0; i < extrasExistentesTemporal.length; i++) {
            for (let j = 0; j < extraSeleccionadosTemporal.length; j++) {
                 if (extrasExistentesTemporal[i].id === extraSeleccionadosTemporal[j].id) {
                    contador++;
                    extraSeleccionadosTemporal.splice(j, 1);
                    extrasExistentesTemporal.splice(i, 1);
                    j--;
                    i--;
                    j = extraSeleccionadosTemporal.length;
                 }
            }
          }
          if (contador === extrasSeleccionadosProducto.length) {
              productoCarrito.cantidad += producto.cantidad;
              productoCarrito.total = (Number(productoCarrito.cantidad) * Number(productoCarrito.valor_temporal));
              insertar = false;
              break;
          }
        }
    }

    // Validación si se crea un producto o se actualiza la cantidad y sus valores.
    if (insertar) {
      let valor = producto.valor_temporal;
      if (producto.sin_valor) {
        valor = 0;
      }
      const productoCarrito  = {'id': producto.id,
                                'cantidad': producto.cantidad,
                                'descripcion': producto.descripcion,
                                'extras': [],
                                'nombre': producto.nombre,
                                'total' : producto.total,
                                'valor_temporal': producto.valor_temporal,
                                'valor': valor};
      this.insertarExtrasProducto(productoCarrito);
      productosCarrito.push(productoCarrito);
    }

    total_orden = productosCarrito.reduce((a, b) => a + b.total, 0);
    this.carritoCompras.total = total_orden;

    this.carritoCompras.productos.forEach(producto => {
      this.cantidadCarrito += Number(producto.cantidad);
      localStorage.setItem('cantidad_carritos' + this.localNombre, this.cantidadCarrito.toString());
    });
    localStorage.setItem(this.localNombre, JSON.stringify(this.carritoCompras));
    modal.close();
    this.carrito.emit({mensaje: producto.nombre, cantidad: producto.cantidad, valor: producto.valor_temporal});
  }


  insertarExtrasProducto(producto) {
    for (let i = 0; i < this.extrasSeleccionados.length; i++) {
      const extraSeleccionado = this.extrasSeleccionados[i];
      if (extraSeleccionado.grupo_extra !== undefined) {
        producto.extras.push({
                          'id' : extraSeleccionado.id,
                          'id_extra_proveedor': 0,
                          'nombre': extraSeleccionado.nombre,
                          'valor': extraSeleccionado.valor});
      } else {
        for (let j = 0; j < extraSeleccionado.length; j++) {
            const extra = extraSeleccionado[j];
            if (extra.seleccionado) {
            producto.extras.push({
                          'id' : extra.id,
                          'id_extra_proveedor': 0,
                          'nombre': extra.nombre,
                          'valor': extra.valor});
            }
        }
      }
    }
  }

  /* seleccionarExtra(seleccion, extraSeleccionado, grupo_extra) {
    let valor = Number(this.productoSeleccionado.valor);
     for (let i = 0; i < this.extrasSeleccionados.length; i++) {
       const extra = this.extrasSeleccionados[i][0];
       if (extra !== undefined) {
         valor += Number(extra.valor);
       }
    }

    this.productoSeleccionado.valor_temporal = valor;
    this.productoSeleccionado.total = (Number(this.productoSeleccionado.cantidad) * Number(this.productoSeleccionado.valor_temporal));
    extraSeleccionado.grupo_extra = grupo_extra;


    if (seleccion) {
        if (this.productoSeleccionado.sin_valor) {
          this.productoSeleccionado.valor_temporal = extraSeleccionado.valor;
          this.productoSeleccionado.total += (this.productoSeleccionado.cantidad * extraSeleccionado.valor);
        } else {
          this.productoSeleccionado.total += (this.productoSeleccionado.cantidad * extraSeleccionado.valor);
        }
        extraSeleccionado.grupo_extra = grupo_extra;
    } else {
        console.log(seleccion);
    }
  } */

  setearGrupoExtra(grupoExtra, extraSeleccionado) {
    console.log(grupoExtra);
    extraSeleccionado.grupo_extra = grupoExtra;
    let valor = Number(this.productoSeleccionado.valor);
    if (this.productoSeleccionado.sin_valor) {
      valor = 0;
    }
     for (let i = 0; i < this.extrasSeleccionados.length; i++) {
       const extra = this.extrasSeleccionados[i];
       if (extra.grupo_extra !== undefined) {
         valor += Number(extra.valor);
       } else {
         if (extra.length !== 0) {
          for (let j = 0; j < extra.length; j++) {
              if (extra[j].seleccionado) {
                  valor += Number(extra[j].valor);
              }
          }
         }
       }
    }
    this.productoSeleccionado.valor_temporal = valor;
    this.productoSeleccionado.total = (Number(this.productoSeleccionado.cantidad) * valor);

  }

  validarAgregarCarrito() {
    if (this.productoSeleccionado.grupo_extras.length > 0) {
    let cont = 0;
    for (let i = 0; i < this.extrasSeleccionados.length; i++) {
      if (this.extrasSeleccionados[i].grupo_extra !== undefined) {
        cont++;
      }
    }

    return (this.productoSeleccionado.grupo_extras.filter( x => x.tipo === 1).length !== cont ||
    this.productoSeleccionado.cantidad < 1 ||
    this.productoSeleccionado.cantidad === null);

    } else {
      return false;
    }
  }

  // Metodos Sidebar
  public abrirToogle(seleccion?): void {
    this._opened = !this._opened;
  }

  public setDireccion(direccion) {
    this.direccion = direccion;
  }

  public setMenu(menu) {
    this.ngxService.start();
    this.categorias = [];
    this.categoriasCompletas = [];
    this.cargarProducto(menu);
  }
  // Fin Metodo Sidebar

  verDetalle(producto) {
    producto.ver_detalle = !producto.ver_detalle;

    if ( producto.ver_detalle ) {
      producto.texto_detalle = 'Ocultar';
      producto.icono_detalle = 'fa-chevron-up';
    } else {
      producto.texto_detalle = 'Detalle';
      producto.icono_detalle = 'fa-chevron-down';
    }

  }

  vaciarCarrito() {
    this.funciones.swalDosBotones(
      'Vaciar Carrito',
      '¿Deseas vaciar el carrito?',
      'question',
      'Si, Vaciar',
      'No'
  ).then(
      result => {
          if (result.value) {
            this.cantidadCarrito = 0;
            this.carritoCompras.productos  = [];
            localStorage.setItem(this.localNombre, JSON.stringify(this.carritoCompras));
            localStorage.setItem('cantidad_carritos' + this.localNombre, '0');
            this.abrirToogle();
          }
      }
  );
  }

  setDatosCarrito() {
    this.celular = this.carritoCompras.celular;
    this.tiempo_entrega = '30 - 35 min';
    const usuario = JSON.parse(localStorage.getItem('usuario'));

    this.total_tu_pago = Number(usuario.tupago);
    this.total_domicilios = Number(this.carritoCompras.comercio.valor_domicilio);
    this.total_productos = Number(this.carritoCompras.total);

    this.total_a_pagar = this.total_productos + this.total_domicilios + this.propina;
  }

  setIniciarCarrito(carrito) {
    this.carritoCompras = carrito;
  }

  validarCarritoCompras() {
    this.ngxService.start();
    this.carritoCompras = JSON.parse(localStorage.getItem(this.proveedorId + this.servicioId));
    this.http.post('validarcarrito', this.carritoCompras, true).subscribe(
      response => {
        if (!response.error) {
          this.validarCarrito = false;
          this.continuarCarrito = true;
          this.setDatosCarrito();
          } else {
            this.funciones.mostrarSwal(
                'Ups, algo salio mal',
                response.message,
                'warning');
          }
          this.ngxService.stop();
      }, error => {
            this.funciones.mostrarCargando('Ups, tenemos problemas, por favor refresca la pagina!');
      });
  }

  medioPago(seleccion, modal?) {
    // Efectivo
    if (seleccion === 1) {
      this.medio = 1;
      this.tarjetaUsuarioSelaccionada = null;
      this.carritoCompras.pago = 1;
      localStorage.setItem(this.localNombre, JSON.stringify(this.carritoCompras));
    }
    // Tarjeta
    if (seleccion === 2) {
      this.ngxService.start();
      this.medio = 2;
      this.cargarTarjetasUsuario();
      this.ngxService.stop();
      this.modalService.open(modal, {centered: true, size: 'lg'});
    }

    // PSE
    if (seleccion === 3) {
      this.medio = 3;
      this.tarjetaUsuarioSelaccionada = null;
    }
  }

  pagarTuPago() {
      this.tuPagoSeleccion = !this.tuPagoSeleccion;
      if (this.tuPagoSeleccion) {
        this.tu_pago = this.total_a_pagar;
        this.total_tu_pago = this.total_tu_pago - this.total_a_pagar;
        this.total_a_pagar = 0;
      } else {
        this.total_a_pagar = this.tu_pago;
        this.tu_pago = 0;
        this.total_tu_pago = this.total_tu_pago + this.total_a_pagar;
      }
  }

  restarPropina() {
    if ( this.propina !== 0) {
       // Si tu pago está chekeado
       if (this.tuPagoSeleccion) {
        this.tu_pago -= 1000;
        this.total_tu_pago += 1000;
      } else {
        this.total_a_pagar -= 1000;
      }
      this.propina = this.propina - 1000;
    }
  }

  sumarPropina() {
    if (this.propina !== 10000) {
      this.propina = this.propina + 1000;
      // Si tu pago está chekeado
      if (this.tuPagoSeleccion) {
        this.tu_pago += 1000;
        this.total_tu_pago -= 1000;
      } else {
        this.total_a_pagar += 1000;
      }
    }
  }

  volverCarrito() {
    this.validarCarrito = true;
    this.continuarCarrito = false;
  }

  enviarPedido() {
    this.carritoCompras = JSON.parse(localStorage.getItem(this.proveedorId + this.servicioId));
    if (this.carritoCompras.direccion.id === '' || this.carritoCompras.direccion.id === null) {
      this.funciones.toast().fire({
        icon: 'warning',
        title: 'Seleccionar dirección!'
      });
      this.direccionEmit.emit();
    } else {
      this.ngxService.start();
      const apiKey = localStorage.getItem('apikey');
      const apiLogin = localStorage.getItem('apilogin');
      this.http.post('ordenar?apiKey=' + apiKey + '&apiLogin=' + apiLogin,  this.carritoCompras, true).subscribe(
        response => {
          if (!response.error) {
            this.abrirToogle();
            this.obtenerInformacionOrden(response.orden_id, true);

            // Reiniciar Variables Carrito
            this.cantidadCarrito = 0;
            this.carritoCompras.productos = [];
            this.carritoCompras.total = 0;
            this.validarCarrito = true;
            this.continuarCarrito = false;
            localStorage.setItem(this.proveedorId + this.servicioId, JSON.stringify(this.carritoCompras));
            localStorage.setItem('cantidad_carritos' + this.localNombre, '0');

            this.funciones.toast().fire({
              icon: 'success',
              title: 'Orden Realizada Correctamente'
            });

            this.obtenerUsuario();
          } else {
            this.funciones.toast().fire({
              icon: 'warning',
              title: response.message
            });
          }
          this.ngxService.stop();
        }, error => {
          this.ngxService.stop();
        });
    }
  }

  obtenerInformacionOrden(idOrden, enviarPedido, modal?) {
    const apiKey = localStorage.getItem('apikey');
    const apiLogin = localStorage.getItem('apilogin');
    let valor_temporal = 0;
      this.http.get('orden?apiKey=' + apiKey + '&apiLogin=' + apiLogin + '&id=' + idOrden, true).subscribe(
        response => {
          if  (!response.error) {
            if (response.message.estado !== 3) {
              if (enviarPedido) {
                this.compraOrden = response.message;
                this.compraOrden.productos.forEach(producto => {
                    producto.extras.forEach(extra => {
                      valor_temporal += Number(extra.valor);
                    });
                });
                this.compraOrden.valor_temporal = valor_temporal;
                localStorage.setItem('compra' + this.localNombre, JSON.stringify(this.compraOrden));
              } else {
                this.compraOrden = response.message;
                this.compraOrden.productos.forEach(producto => {
                  producto.extras.forEach(extra => {
                    valor_temporal += Number(extra.valor);
                  });
                });
                this.compraOrden.valor_temporal = valor_temporal;
                // console.log(this.compraOrden);
              }
            } else {

                this.compraOrden = response.message;
                this.compraOrden.productos.forEach(producto => {
                    producto.extras.forEach(extra => {
                      valor_temporal += Number(extra.valor);
                    });
                });
                this.compraOrden.valor_temporal = valor_temporal;
                localStorage.setItem('compra', '');
                // console.log(this.compraOrden);
            }
            //console.log('entre' + modal);
            if  (modal !== null) {
              if(modal !== undefined){
                this.modalService.open(modal, {centered: true, size: 'lg'});
              }
            }
          } else {
            this.funciones.toast().fire({
              icon: 'warning',
              title: response.message
            });
          }
          this.ngxService.stop();
        }, err => {
          this.funciones.toast().fire({
            icon: 'warning',
            title: 'Ups, tenemos un error al momento de procesar la información.'
          });
          this.ngxService.stop();
        }
      );
  }

  // Metodos pago en tarjeta
  seleccionarTarjeta(tarjeta, modal) {
    this.carritoCompras.pago = tarjeta.id;
    this.tarjetaUsuarioSelaccionada = tarjeta;
    localStorage.setItem(this.proveedorId + this.servicioId, JSON.stringify(this.carritoCompras));
    modal.close();
  }

  eliminarTarjeta(tarjeta) {
    const apiKey = localStorage.getItem('apikey');
    const apiLogin = localStorage.getItem('apilogin');

    let formData: any = new FormData();
    formData.append('apiLogin', apiLogin);
    formData.append('apiKey', apiKey);
    formData.append('tarjeta_id', tarjeta.id);

    this.http.post('del_tarjeta', formData, true).subscribe(
      response => {
        if (!response.error) {
          this.tarjetaUsuarioSelaccionada = null;
          const index = this.tarjetasUsuario.findIndex(x => x.id = tarjeta.id);
          this.tarjetasUsuario.splice(index, 1);
          this.funciones.toast().fire({
            icon: 'success',
            title: 'Tarjeta eliminada correctamente!'
          });

         } else {
            this.funciones.toast().fire({
              icon: 'warning',
              title: 'Error eliminando la tarjeta!'
            });
          }
      }, error => {
        this.funciones.toast().fire({
          icon: 'warning',
          title: 'Error eliminando la tarjeta!'
        });
      });

  }

  public getError(controlName: string, form): string {
    let error = '';
    const control = form.get(controlName);
    if (control.errors != null) {
        error = JSON.stringify(control.errors);
    }
    return error;
    // return '';
  }

  cargarTarjetasUsuario() {
    const apiKey = localStorage.getItem('apikey');
    const apiLogin = localStorage.getItem('apilogin');
    this.http.get('tarjetas?apiKey=' + apiKey + '&apiLogin=' + apiLogin, true).subscribe(
      response => {
        if  (!response.error) {
          this.tarjetasUsuario = response.message;
          if (this.tarjetasUsuario.length === 0) {
            this.funciones.toast().fire({
              icon: 'success',
              title: 'No tienes ninguna tarjeta guardada!'
            });
          }
          this.ngxService.stop();
        }
      }, err => {
      }
    );
  }

  guardarTarjeta(modalACerrar, modalAAbrir) {
    this.ngxService.start();
    const apiKey = localStorage.getItem('apikey');
    const apiLogin = localStorage.getItem('apilogin');
    const tipo = this.form_creacion_tarjeta.get('tipo_documento').value[0].id;
    const documento = this.form_creacion_tarjeta.get('documento').value;
    const numero = this.form_creacion_tarjeta.get('numero').value;
    const fecha_expiracion = this.form_creacion_tarjeta.get('fecha_expiracion').value;
    const cvv = this.form_creacion_tarjeta.get('cvv').value;
    const nombre = this.form_creacion_tarjeta.get('nombre').value;
    const apellido = this.form_creacion_tarjeta.get('apellido').value;
    const correo = this.form_creacion_tarjeta.get('correo').value;

      let formData: any = new FormData();
      formData.append('apiLogin', apiLogin);
      formData.append('apiKey', apiKey);
      formData.append('tipo_documento', tipo);
      formData.append('documento', documento);
      formData.append('numero', numero);
      formData.append('fecha_expiracion', fecha_expiracion);
      formData.append('cvv', cvv);
      formData.append('nombre', nombre);
      formData.append('apellido', apellido);
      formData.append('correo', correo);

    this.http.post('tarjeta',  formData, true).subscribe(
      response => {
          if (!response.error) {

            this.form_creacion_tarjeta  = this.fb.group({
              tipo_documento : ['', Validators.required],
              documento: ['', Validators.required],
              numero : ['', Validators.required],
              fecha_expiracion : ['', [Validators.required, Validators.maxLength(7)]],
              cvv : ['', Validators.required],
              nombre : ['', Validators.required],
              apellido : ['', Validators.required],
              correo : ['', Validators.required],
            });

            this.cargarTarjetasUsuario();
            modalACerrar.dismiss();
            this.modalService.open(modalAAbrir, {centered: true, size: 'lg'});

            this.funciones.toast().fire({
              icon: 'success',
              title: 'Tarjeta Guardada correctamente!'
            });
          } else {
            this.funciones.toast().fire({
              icon: 'warning',
              title: 'Error guardando la tarjeta!'
            });
          }
          this.ngxService.stop();
      }, error => {
        this.funciones.toast().fire({
          icon: 'warning',
          title: 'Error guardando la tarjeta!'
        });
        this.ngxService.stop();

      });
  }

  volverAtrasTarjeta(modalACerrar, modalAbrir) {
      modalACerrar.dismiss();
      this.modalService.open(modalAbrir, {centered: true, size: 'lg'});
  }

  validarFormatoFecha(event) {
    if (event.target.value.length === 2) {
      const mes = this.form_creacion_tarjeta.get('fecha_expiracion').value.toString();
      this.form_creacion_tarjeta.get('fecha_expiracion').setValue(mes + '/');
    } else {

    }
  }

  ordenarNuevamente() {
    this.carritoCompras.productos = [];
    this.carritoCompras.total = 0;
    localStorage.setItem(this.localNombre, JSON.stringify(this.carritoCompras));
    localStorage.setItem('cantidad_carritos' + this.localNombre, '0');

    this.validarCarrito = true;
    this.cantidadCarrito = 0;
  }

  abrirModalLogin() {
    this.modalService.dismissAll();
    this.login.emit();
  }

  filtros() {
    const that = this;
    if (this.busquedaProductos !== null && this.busquedaProductos !== '') {
        this.categorias = this.categoriasCompletas.filter(function (categoria) {
                      categoria.productos = categoria.productosCompletos.filter(producto =>
                                              (producto.nombre).toLocaleLowerCase().indexOf(that.busquedaProductos.toLowerCase()) > -1);
                                              return categoria.productos.length !== 0;
                                  }
          );
    } else {
      this.categorias = this.categoriasCompletas.filter(function (categoria) {
        categoria.productos = categoria.productosCompletos.filter(producto =>
                                (producto.nombre).toLocaleLowerCase().indexOf(that.busquedaProductos.toLowerCase()) > -1);
                                return categoria.productos.length !== 0;
                    });
    }
  }

  obtenerUsuario() {
    const apiKey = localStorage.getItem('apikey');
    const apiLogin = localStorage.getItem('apilogin');
    // this.http.get('usuario?apiKey=' + apiKey + '&apiLogin=' + apiLogin, true).subscribe(
      this.http.get('usuario', true).subscribe(
      response => {
        if ( !response.error) {
          this.usuario = response.message;
          localStorage.setItem('usuario', JSON.stringify(this.usuario));
        }
      }, err => {
        this.ngxService.stop();
      });
  }
}
