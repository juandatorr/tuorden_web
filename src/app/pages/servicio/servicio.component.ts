import { Component, OnInit, Output, EventEmitter, ViewEncapsulation, Inject } from '@angular/core';

// core components
import { ConexionLaravelHttpService } from 'src/app/shared/services/conexion-laravel-http.service';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Validators, FormBuilder } from '@angular/forms';
import { FuncionesComunesService } from 'src/app/shared/services/funciones-comunes.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { HttpHeaders } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import { MapsAPILoader } from '@agm/core';
import { log } from 'console';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-servicio',
  templateUrl: './servicio.component.html',
  styleUrls: ['./servicio.component.scss'],
})
export class ServicioComponent implements OnInit {

  @Output() visualizarEmit: EventEmitter<any> = new EventEmitter<any>();

  configuracionSelect = {};
  visualizarServicios = true;

  // Atributos servicio
  busquedaServicio = '';
  servicios = [];
  serviciosCompletos = [];
  // Fin Atributos servicio

  // Atributos comercio
  busquedaComercio = '';
  comercios = [];
  comerciosCompletos = [];
  // Fin Atributos comercio

  imageObject: Array<object> = [];


  carritoCompras;
  constructor(private http: ConexionLaravelHttpService,
              public config: NgbModalConfig,
              public fb: FormBuilder,
              public funciones: FuncionesComunesService,
              public modalService: NgbModal,
              private ngxService: NgxUiLoaderService,
              @Inject(DOCUMENT) document: any,
              private route: ActivatedRoute,
              private router: Router
              ) {

                const ordenJson = localStorage.getItem('orden');
                if (ordenJson === null) {
                  this.crearOrden();
                }

                this.cargarPromociones();
                this.visualizar();
                this.cargarSelect();
                this.cargarServicios();
              }

    ngOnInit() {}


  cargarSelect() {
    this.configuracionSelect = {
      singleSelection: true,
      text: 'Selecciona una opción',
      classes: 'myclass',
      primaryKey: 'id',
      labelKey: 'nombre',
      noDataLabel: 'No se encontraron datos',
      enableSearchFilter: true,
      enableFilterSelectAll: false,
      searchPlaceholderText: 'Buscar',
      // lazyLoading: true,
    };
  }


  cargarProductosComercio() {
    this.http.get('menu_sede?proveedor=', true).subscribe(
      response => {
      }, err => {
      }
    );
  }

  visualizarProductos(comercio, promocion?) {
    // console.log(promocion);

    const orden = JSON.parse(localStorage.getItem('orden'));
    const visualizar = JSON.parse(localStorage.getItem('visualizar'));


    if (promocion) {
      const comercioFilter = this.comercios.filter( x => x.id = promocion.proveedor_id);
      console.log(comercioFilter);

      if (comercioFilter.length !== 0) {
        comercio = comercioFilter[0];
        comercio.promocion = promocion;
      }
    }

    orden.comercio = comercio;

    localStorage.setItem('orden', JSON.stringify(orden));

    localStorage.setItem(comercio.id + '' + visualizar.servicio.id, JSON.stringify(orden));
    localStorage.setItem('nombre_proveedor', comercio.nombre);
    localStorage.setItem('id_proveedor', comercio.id);
    this.router.navigate(['/home']);
  }


  // Metodos de los servicios
  irServicio(servicio) {
    if (servicio.id === 1) {
      this.cargarPromociones();
      this.cargarComercios(servicio.id);
      this.visualizarServicios = false;
      localStorage.setItem('visualizar', '{"visualizar": ' + this.visualizarServicios + ', "servicio": ' + JSON.stringify(servicio) + '}');
      this.visualizarEmit.emit('header');
    } else {
      this.funciones.toast().fire({
        icon: 'success',
        title: servicio.nombre
      });
    }

    const orden = JSON.parse(localStorage.getItem('orden'));
    orden.servicio_id = servicio.id;
    localStorage.setItem('orden', JSON.stringify(orden));
  }

  filtrosServicios() {
    if (this.busquedaServicio.toLowerCase() !== null && this.busquedaServicio.toLowerCase() !== '') {
        this.servicios = this.serviciosCompletos.filter(
            x =>
                (x.nombre).toLowerCase().indexOf(this.busquedaServicio.toLowerCase()) > -1 ||
                (x.direccion + '').toLowerCase().indexOf(this.busquedaServicio.toLowerCase()) > -1 ||
                (x.id + '').toLowerCase().indexOf(this.busquedaServicio.toLowerCase()) > -1
        );
    } else {
        this.servicios = this.serviciosCompletos;
    }
  }

  cargarServicios(evento?) {

    if (evento != null) {

      const usu = JSON.parse(localStorage.getItem('usuario'));
      if (usu.prime) {
          const index = evento.findIndex(x => x.id === 43);
          index.splice(index, 1);
      }

      const indexTu = evento.findIndex(x => x.id === 1);

      this.servicios = [evento[indexTu]];
      this.serviciosCompletos = [evento[indexTu]];
    } else {
      this.servicios.push({id : 1, nombre: 'Tu Comida'});
      // this.servicios.push({id : 2, nombre: 'Tu Encargo'});
      // this.servicios.push({id : 6, nombre: 'Tu Transporte'});
      // this.servicios.push({id : 10, nombre: 'Tu Mercado'});
      // this.servicios.push({id : 11, nombre: 'Tu Salud'});
      // this.servicios.push({id : 12, nombre: 'Tu Compañero'});
      // this.servicios.push({id : 16, nombre: 'Tu Take Out'});
      // this.servicios.push({id : 18, nombre: 'Tu Comercio'});
      // this.servicios.push({id : 19, nombre: 'Tu Factura'});
      // this.servicios.push({id : 20, nombre: 'Tu Plan Portero'});
      // this.servicios.push({id : 21, nombre: 'Tu Donación'});

      this.serviciosCompletos.push({id : 1, nombre: 'Tu Comida'});
      // this.serviciosCompletos.push({id : 2, nombre: 'Tu Encargo'});
      // this.serviciosCompletos.push({id : 6, nombre: 'Tu Transporte'});
      // this.serviciosCompletos.push({id : 10, nombre: 'Tu Mercado'});
      // this.serviciosCompletos.push({id : 11, nombre: 'Tu Salud'});
      // this.serviciosCompletos.push({id : 12, nombre: 'Tu Compañero'});
      // this.serviciosCompletos.push({id : 16, nombre: 'Tu Take Out'});
      // this.serviciosCompletos.push({id : 18, nombre: 'Tu Comercio'});
      // this.serviciosCompletos.push({id : 19, nombre: 'Tu Factura'});
      // this.serviciosCompletos.push({id : 20, nombre: 'Tu Plan Portero'});
      // this.serviciosCompletos.push({id : 21, nombre: 'Tu Donación'});
    }


  }
  // Fin método de servicios.

  // Metodos de los comercios
  filtrosComercios() {
    /* const that = this;
    if (this.busquedaComercio !== null && this.busquedaComercio !== '') {
        this.comercios = this.comerciosCompletos.filter(function (categoria) {
                      categoria.comercios = categoria.comerciosCompletos.filter(comercio =>
                                              (comercio.nombre).toLocaleLowerCase().indexOf(that.busquedaComercio.toLowerCase()) > -1);
                                              return categoria.comercios.length !== 0;
                                  }
          );
    } else {
      this.comercios = this.comerciosCompletos.filter(function (categoria) {
        categoria.comercios = categoria.comerciosCompletos.filter(comercio =>
                                (comercio.nombre).toLocaleLowerCase().indexOf(that.busquedaComercio.toLowerCase()) > -1);
                                return categoria.comercios.length !== 0;
                    });
    } */

    if (this.busquedaComercio.toLowerCase() !== null && this.busquedaComercio.toLowerCase() !== '') {
      this.comercios = this.comerciosCompletos.filter(
          x =>
              (x.nombre).toLowerCase().indexOf(this.busquedaComercio.toLowerCase()) > -1 ||
              // (x.direccion + '').toLowerCase().indexOf(this.busquedaComercio.toLowerCase()) > -1 ||
              (x.id + '').toLowerCase().indexOf(this.busquedaComercio.toLowerCase()) > -1
      );
  } else {
      this.comercios = this.comerciosCompletos;
  }
  }

  cargarComercios(servicioId) {

    this.ngxService.start();
    const direccionSeleccionada = JSON.parse(localStorage.getItem('direccion_seleccionada'));
    const lat = direccionSeleccionada.latitud;
    const lng = direccionSeleccionada.longitud;
    const apiKey = localStorage.getItem('apikey');
    const apiLogin = localStorage.getItem('apilogin');

    this.http.get('proveedores?latitud=' + lat + '&longitud=' + lng + '&servicio_id=' + servicioId + '&apiLogin=' + apiLogin + '&apiKey=' + apiKey, true).subscribe(
      response => {
        if (!response.error) {
          this.comercios = response.message;
          this.comerciosCompletos = response.message;
          this.ngxService.stop();
        } else {
          this.ngxService.stop();
        }
      }, err => {
        this.ngxService.stop();
      }
    );


    /*this.comercios.push({id: 0, nombre: 'Todos', comercios : []});
    this.comercios.push({id: 1, nombre: 'Comida de mar', comercios: [{id: 1, nombre: 'La Lonchera', descripcion : 'Sushi, Comida Internacional, Nuevos Restaurantes', direccion: 'Carrera 19 # 71-11'}] });
    this.comercios.push({id: 1, nombre: 'Pizza', comercios: [{id: 583, nombre: 'La Nostra Pizza', descripcion : 'Comida, Comida, Comida, Comida', direccion: 'Carrera 19 # 71-11'}] });
    this.comercios.push({id: 1, nombre: 'Pollo', comercios: [{id: 296, nombre: 'Mister Lee', descripcion : 'Comida, Comida, Comida, Comida', direccion: 'Carrera 19 # 71-11'}] });
    this.comercios.push({id: 1, nombre: 'Oriental', comercios: [{id: 143, nombre: 'PPC', descripcion : 'Comida, Comida, Comida, Comida', direccion: 'Carrera 19 # 71-11'}] });

    this.comerciosCompletos.push({id: 0, nombre: 'Todos', comercios : []});
    this.comerciosCompletos.push({id: 1, nombre: 'Comida de mar', comercios: [{id: 1, nombre: 'La Lonchera', descripcion : 'Sushi, Comida Internacional, Nuevos Restaurantes', direccion: 'Carrera 19 # 71-11'}] });
    this.comerciosCompletos.push({id: 1, nombre: 'Pizza', comercios: [{id: 583, nombre: 'La Nostra Pizza', descripcion : 'Comida, Comida, Comida, Comida', direccion: 'Carrera 19 # 71-11'}] });
    this.comerciosCompletos.push({id: 1, nombre: 'Pollo', comercios: [{id: 296, nombre: 'Mister Lee', descripcion : 'Comida, Comida, Comida, Comida', direccion: 'Carrera 19 # 71-11'}] });
    this.comerciosCompletos.push({id: 1, nombre: 'Oriental', comercios: [{id: 143, nombre: 'PPC', descripcion : 'Comida, Comida, Comida, Comida', direccion: 'Carrera 19 # 71-11'}] });
    */
    for (let j = 0; j < this.comerciosCompletos.length; j++) {
      this.comerciosCompletos[j].comerciosCompletos = this.comercios[j].comercios;
    }
  }

  filtrarComerciosCategoria(categoria) {
    if (categoria.id === 0) {
      this.comercios = this.comerciosCompletos;
    } else {
      this.comercios = [categoria];
    }
  }
  // Fin Metodos de los comercios

  agregarCarrito(seleccionado) {
      console.log(seleccionado);
  }

  visualizar() {
    const visualizar = JSON.parse(localStorage.getItem('visualizar'));
    if (visualizar !== null) {
      this.visualizarServicios = visualizar.visualizar;
      if (visualizar.servicio !== undefined) {
        this.irServicio(visualizar.servicio);
      }
    } else {
      this.visualizarServicios = true;
      localStorage.setItem('visualizar', '{"visualizar": ' + true + '}');
    }
  }

  cargarPromociones() {
    // https://tuorden.com.co/api/app/v1.18/promociones_proveedores?latitud=4.6869268&longitud=-74.0603598&servicio_id=1&limite=10
    const direccionSeleccionada = JSON.parse(localStorage.getItem('direccion_seleccionada'));

    if (direccionSeleccionada != null) {
      this.ngxService.start();
      const lat = direccionSeleccionada.latitud;
      const lng = direccionSeleccionada.longitud;
      const servicio = JSON.parse(localStorage.getItem('visualizar'));
      let idServicio = 1;
      if ( servicio.servicio !== undefined) {
        idServicio = servicio.servicio.id;
      }
      this.http.get('promociones_proveedores?latitud=' + lat + '&longitud=' + lng + '&servicio_id=' + idServicio + '&limite=10', true).subscribe(
        response => {
          if (!response.error) {
            response.message.forEach(promo => {
                promo.image = 'https://tuorden.com.co/web/image_producto/img-small-' + promo.id + '.jpeg';
                promo.thumbImage = 'https://tuorden.com.co/web/image_producto/img-small-' + promo.id + '.jpeg';
                promo.alt = '' + promo.nombre;
                promo.title = '' + promo.nombre;
                promo.proveedor = promo.proveedor_id;
                promo.id_promo = promo.id;
            });
            this.imageObject = response.message;
            this.ngxService.stop();
          }
          this.ngxService.stop();
        }, err => {
          this.ngxService.stop();
        }
      );
    }
    }

    // Método para crear una orden
  crearOrden() {
    this.carritoCompras = {celular: '', comentario: '', comercio: {
                          comentarios: true,
                          descripcion: '',
                          direccion: '',
                          fecha_modificacion: '',
                          id: '',
                          latitud: '',
                          longitud: '',
                          marketplace: false,
                          nombre: '',
                          sede_id: '',
                          takeOut: false,
                          valor_domicilio: ''
                      }, direccion: {
                                    direccion: '',
                                    descripcion: '',
                                    id: '',
                                    latitud: '',
                                    longitud: '',
                                    nombre: ''
                      },
                      estado_entrega: 0,
                      fecha_programacion: '',
                      id: 0,
                      marketplace: false,
                      pago: 1,
                      productos: [],
                      propina : 0,
                      servicio_id : 1,
                      take_out : false,
                      total: 0,
                      tu_pago : 0,
                      sistema: {
                      app: {
                            builType : 'release',
                            version : 0,
                      },
                      os: {
                            name: '',
                            model: '',
                            sdk: '',
                            version: ''
                      }
                      }
                    };

      localStorage.setItem('orden', JSON.stringify(this.carritoCompras));
  }


}
